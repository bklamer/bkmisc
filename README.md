# bkmisc

`bkmisc` provides miscellaneous functions for data analysis.

## Installation

devtools::install_bitbucket("bklamer/bkmisc")

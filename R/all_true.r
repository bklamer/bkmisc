#' Are all values True?
#'
#' Returns `TRUE` if all values in a vector are `TRUE`.
#'
#' You might want to use something like`all(x == 5)`, but if `x` contains `NA`s,
#' the returned Boolean will be `NA`.
#'
#' @param x A vector.
#'
#' @return `TRUE` or `FALSE`
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # all_true() examples
#' #----------------------------------------------------------------------------
#' library(bkmisc)
#'
#' @export
all_true <- function(x) {
  # Often use >, <, or == inside all(). But that doesn't handle NAs well.
  # here is a solution
  isTRUE(all(x))
}

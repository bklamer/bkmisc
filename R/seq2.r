#' Nondecreasing integer sequence
#'
#' Similar to `seq.int`, but returns an empty vector if the starting point is larger than the end point.
#'
#' @param from An integer.
#' @param to An integer.
#'
#' @return integer vector
#'
#' @export
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # seq2() examples
#' #----------------------------------------------------------------------------
#' library(bkmisc)
#'
#' seq2(from = 1, to = 5)
#' seq2(from = 1, to = 1)
#' seq2(from = 1, to = 0)
#'
seq2 <- function(from, to) {
  if(from > to) {
    return(integer(0))
  }
  seq.int(from = from, to = to)
}

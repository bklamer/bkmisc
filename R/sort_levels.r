#' Sort levels of a factor from most observed to least observed
#'
#' Sort levels of a factor from most observed to least observed. Useful for
#' truly categorical data when you wish to plot it with visually monotonic
#' barplots. If there are ties, then it sorts the tie groups alphabetically.
#'
#' @param x A vector of class factor.
#' @param desc TRUE or FALSE. TRUE goes from most to least observed
#' (descending). FALSE goes from least to most observed (ascending).
#' @param desc_ties TRUE or FALSE. If there are ties in counts, then TRUE sorts
#' those groups from z to a (descending), and FALSE sorts from a to z
#' (ascending).
#'
#' @return factor vector of length x
#'
#' @importFrom bkdat group_by arrange_ desc
#'
#' @export
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # sort_levels() examples.
#' #----------------------------------------------------------------------------
#' library(bkmisc)
#'
#' # case for ties in counts
#' tmp <- factor(c("a", "a", "a", "b", "b", "b", "c"), levels = c("c", "b", "a"))
#' tmp
#' sort_levels(tmp, desc = TRUE, desc_ties = FALSE)
#' sort_levels(tmp, desc = TRUE, desc_ties = TRUE)
#' sort_levels(tmp, desc = FALSE, desc_ties = FALSE)
#' sort_levels(tmp, desc = FALSE, desc_ties = TRUE)
#'
#' # no ties in counts
#' tmp <- factor(c("a", "a", "a", "b", "b", "c"), levels = c("c", "b", "a"))
#' tmp
#' sort_levels(tmp, desc = TRUE)
#' sort_levels(tmp, desc = FALSE)
#'
sort_levels <- function(x, desc = TRUE, desc_ties = FALSE) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  check_factor(x)
  check_logical(desc)
  check_logical(desc_ties)

  #-----------------------------------------------------------------------------
  # Reorder levels
  #-----------------------------------------------------------------------------
  # Create a dataframe for testing if there are ties in counts
  table <- data.frame(sort(table(x)), stringsAsFactors = FALSE)
  # If there are ties, then sort the levels as specified within those groups.
  if(length(unique(table[["Freq"]])) != nrow(table)) {
    table[["x"]] <- as.character(table$x)
    # First order ties
    # fix this so there is no need for if statement.
    if(desc_ties) {
      table <- group_by(table, "Freq")
      table <- arrange_(table, "desc(x)", by_group = TRUE)
    } else {
      table <- group_by(table, "Freq")
      table <- arrange_(table, "x", by_group = TRUE)
    }
    # Second, order sort based on counts and the grouped tie levels.
    table <- table(factor(x, levels = table[["x"]]))
    levels <- names(sort(table, decreasing = desc))
  } else {
    # If no ties, sort based only on counts.
    levels <- names(sort(table(x), decreasing = desc))
  }

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  factor(x, levels = levels)
}

#' Create a placeholder data.frame
#'
#' Create a placeholder data.frame.
#'
#' @param nrow An integer.
#' @param ncol An integer.
#' @param fill Placeholder to fill dataframe. Defaults to `NA`.
#'
#' @return data.frame
#'
#' @export
#'
#' @examples
#'
#' #----------------------------------------------------------------------------
#' # empty_df() examples
#' #----------------------------------------------------------------------------
#' library(bkmisc)
#'
#' empty_df(6, 3)
#'
empty_df <- function(nrow, ncol, fill = NA) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  check_numeric(nrow)
  check_numeric(ncol)

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  as.data.frame(matrix(fill, nrow, ncol))
}

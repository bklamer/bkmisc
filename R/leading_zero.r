#' Convert integers to zero padded strings
#'
#' Convert integer vector to zero padded character strings. Automatically chooses
#' correct number of zeros.
#'
#' @param x An integer vector.
#' @param n Number of leading zeroes. Defaults to the number of characters of
#'          the largest number in `x`.
#'
#' @return character string
#'
#' @export
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # leading_zero() examples
#' #----------------------------------------------------------------------------
#' library(bkmisc)
#'
#' leading_zero(c(1, 5, 9, 10, 75, 100))
#'
leading_zero <- function(x, n = NULL) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  check_numeric(x)
  if(!all(x >= 1)) {
    stop(
      "Check the x argument in function 'bkmisc::leading_zero()'. x needs to be a positive (>0) integer vector.",
      call. = FALSE
    )
  }
  # Cleanly convert floats to integers
  # This handles negative numbers too for a more general solution elsewhere.
  x <- as.integer(x + sign(x) * .5)

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  if(is.null(n)) {
    # This handles negative numbers too.
    n <- max(nchar(trunc(abs(x))))
  }
  sprintf(sprintf("%%0%dd", n), x)
}

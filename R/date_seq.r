#' Create sequential datetimes in a data frame
#'
#' Suppose you have a longitudinal data frame which contains non-sequential
#' measurements over time. This function will create sequential datetimes by
#' filling in the missing rows of datetimes (from the minimum datetime to the
#' maximum datetime) on a minute, hour, day, week, month, quarter, or year basis.
#' It will summarize variables recorded at a higher resolution than the chosen
#' datetime basis (e.g. go from seconds to minute, minutes to hourly, etc.). The
#' basic process will create a template dataframe of sequential datetimes, then
#' join your original dataframe to this template.
#'
#' @param df A longitudinal data frame.
#' @param date A string for the name of the datetime variable.
#' @param group A character vector of grouping variables (such as subject ID).
#' @param unit A string for the sequential time basis. An element of "second",
#' "minute", "hour", "day", "week", "month", "quarter", and "year".
#' @param summarize A named list of functions to use for summarization. Applies
#' for the case when `date` is duplicated or when rounding to a lower resolution
#' time unit.
#'
#' @return data.frame
#'
#' @export
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # date_seq() examples.
#' #----------------------------------------------------------------------------
#' library(bkmisc)
#'
#' data <- data.frame(
#'   subject = c(1, 1, 1, 1, 2, 2, 2, 2),
#'   date = c(
#'     as.Date("2022-01-01"), as.Date("2022-01-01"), as.Date("2022-01-03"), as.Date("2022-01-08"),
#'     as.Date("2022-01-01"), as.Date("2022-01-01"), as.Date("2022-01-04"), as.Date("2022-01-06")
#'   ),
#'   character = letters[1:8],
#'   factor = factor(c("A", "B", "A", "B", "B", "A", "A", "B")),
#'   logical = c(TRUE, FALSE, TRUE, FALSE, FALSE, TRUE, TRUE, FALSE),
#'   numeric = c(10, 20, 30, 40, 15, 30, 45, 55),
#'   date_other = c(
#'     as.Date("2022-06-01"), as.Date("2022-06-02"), as.Date("2022-06-03"), as.Date("2022-06-04"),
#'     as.Date("2022-06-01"), as.Date("2022-06-02"), as.Date("2022-06-03"), as.Date("2022-06-04")
#'   )
#' )
#'
#' date_seq(df = data, date = "date", group = "subject", unit = "day")
#'
date_seq <- function(
    df,
    date,
    group,
    unit,
    summarize = list(
      numeric = function(x) {mean(x, na.rm = TRUE)},
      character = function(x) {dplyr::first(x)},
      factor = function(x) {dplyr::first(x)},
      logical = function(x) {dplyr::first(x)},
      date = function(x) {min(x, na.rm = TRUE)}
    )
) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  check_data_frame(df)
  check_string(date)
  check_character(group)
  check_string(unit)
  if(!(unit %in% c("second", "minute", "hour", "day", "week", "month",
                   "quarter", "year")
  )) {
    stop(
      "Check the 'unit' argument in function bkmisc::date_seq(). Is it spelled correctly? Must be 'sec', 'min', 'hour', 'day', 'week', 'month', 'quarter', or 'year'.",
      call. = FALSE
    )
  }
  check_named_vector(summarize)

  #-----------------------------------------------------------------------------
  # Summarize variables to time basis
  # 1. Round dates
  # 2. group_by groups and date
  # 3. Summarize variables
  #-----------------------------------------------------------------------------
  col_order <- names(df)
  df_summ <- df |>
    dplyr::mutate(
      "{date}" := lubridate::floor_date(x = {date}, unit = unit)
    ) |>
    dplyr::group_by(dplyr::across(tidyselect::all_of(c(group, date)))) |>
    dplyr::summarize(
      across(where(is.numeric), ~ summarize$numeric(.x)),
      across(where(is.character), ~ summarize$character(.x)),
      across(where(is.factor), ~ summarize$factor(.x)),
      across(where(is.logical), ~ summarize$logical(.x)),
      across(where(bkmisc::is_date), ~ summarize$date(.x))
    ) |>
    dplyr::select(all_of(col_order))

  #-----------------------------------------------------------------------------
  # Get min and max dates per group
  #-----------------------------------------------------------------------------
  df_minmax <- df |>
    dplyr::group_by(dplyr::across(tidyselect::all_of(group))) |>
    dplyr::summarize(
      .min = min(.data[[date]], na.rm = TRUE),
      .max = max(.data[[date]], na.rm = TRUE)
    )

  #-----------------------------------------------------------------------------
  # Create join grid
  #-----------------------------------------------------------------------------
  df_grid <- df_minmax |>
    dplyr::group_by(dplyr::across(tidyselect::all_of(group))) |>
    dplyr::mutate(
      "{date}" := purrr::map2(
        .x = .min,
        .y = .max,
        .f = seq,
        by = unit
      )
    ) |>
    tidyr::unnest(cols = {{date}}) |>
    dplyr::distinct() |>
    as.data.frame() |>
    dplyr::select(-.min, -.max) |>
    dplyr::ungroup()

  #-----------------------------------------------------------------------------
  # Join data
  #-----------------------------------------------------------------------------
  df_join <- df_summ |>
    dplyr::right_join(df_grid, by = c(group, date)) |>
    dplyr::arrange(dplyr::across(tidyselect::all_of(c(group, date)))) |>
    dplyr::ungroup() |>
    as.data.frame()

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  df_join
 }

#' Diagnostic plots for dataframe variables
#'
#' Quickly creates diagnostic plots for dataframe variables.
#'
#' @param df A data.frame
#' @param type A character vector of data types to plot.
#'
#' @return ggplots
#'
#' @importFrom ggplot2 ggplot
#'
#' @export
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # plot_df() examples
#' #----------------------------------------------------------------------------
#' library(bkmisc)
#'
plot_df <- function(df, type) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  check_data_frame(df)

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  ggplot(df)
}

#' Apply a function over selected columns
#'
#' Apply a function over selected columns.
#'
#' Has some coercion limitations. e.g. you cannot convert to factor (it will result in character columns).
#'
#' @param data A data frame.
#' @param select A logical or integer vector representing the column to be
#' selected. `select = TRUE` will select all columns.
#' @param fun The function to be applied to each selected column.
#' @param ... Optional arguments to fun.
#'
#' @return data.frame
#'
#' @export
#'
#' @examples
#'
#' #----------------------------------------------------------------------------
#' # capply() examples
#' #----------------------------------------------------------------------------
#' library(bkmisc)
#'
#' df <- data.frame(factor = factor(c("a", "b")), numeric = c(1, 2))
#'
#' # convert factor column to character
#' capply(df, sapply(df, is.factor), as.character)
#'
capply <- function(data, select, fun = function(x) {}, ...) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  check_data_frame(data)
  check_function(fun)
  check_numeric_or_logical(select)

  #-----------------------------------------------------------------------------
  # Apply on selected columns
  #-----------------------------------------------------------------------------
  data[, select] <- sapply(
    data[, select],
    fun,
    ...,
    simplify = TRUE,
    USE.NAMES = FALSE
  )

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  data
}

#' Print comma separated, quoted values
#'
#' Print a string of comma separated, quoted values.
#'
#' @param x A character vector.
#' @param max An integer.
#' @param quote TRUE or FALSE.
#'
#' @return string
#'
#' @export
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # pretty_print() examples
#' #----------------------------------------------------------------------------
#' library(bkmisc)
#'
#' lapply(list(integer(0), 1L, 1:2, 1:4, 1:5, 1:6, 1:7, 1:10), pretty_print)
#'
pretty_print <- function(x, max = 6, quote = TRUE) {
  if(quote) {
    x <- shQuote(x)
  }
  if(length(x) > max) {
    x <- c(x[seq_len(max)], "...")
  }
  if(quote && length(x) > 1) {
    shQuote(paste0(x, collapse = ", "))
  } else {
    paste0(x, collapse = ", ")
  }
}

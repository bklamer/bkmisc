#' Correlation of a sparse matrix
#'
#' Create a correlation matrix from a sparse matrix.
#'
#' @param x A sparse matrix
#'
#' @return dgCMatrix
#'
#' @references https://stackoverflow.com/a/45655597
#'
#' @importFrom Matrix colMeans colSums tcrossprod crossprod diag
#'
#' @export
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # cor_sparse() examples
#' #----------------------------------------------------------------------------
#' library(bkmisc)
#'
#' x <- sample(0:10, 1e8, replace = TRUE, p = c(0.99, rep(0.001, 10)))
#' x <- Matrix::Matrix(x, ncol = 5)
#'
#' cor_sparse(x)
#'
cor_sparse <- function(x) {
  n <- nrow(x)
  cMeans <- Matrix::colMeans(x)
  cSums <- Matrix::colSums(x)

  # Calculate the population covariance matrix.
  # There's no need to divide by (n-1) as the standard deviation is also
  # calculated the same way.
  covmat <- Matrix::tcrossprod(cMeans, (-2*cSums+n*cMeans))
  crossp <- as.matrix(Matrix::crossprod(x))
  covmat <- covmat + crossp
  sdvec <- sqrt(Matrix::diag(covmat)) # standard deviations of columns

  # Return
  covmat / Matrix::tcrossprod(sdvec) # correlation matrix
}

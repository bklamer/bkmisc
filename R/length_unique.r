#' Length of vectors unique values
#'
#' Length of vectors unique values (including factors).
#'
#' @param x A vector
#'
#' @return Integer vector of length 1.
#'
#' @export
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # length_unique() examples
#' #----------------------------------------------------------------------------
#' library(bkmisc)
#'
#' length_unique(10)
#' length_unique(c(1, 2, 3))
#' length_unique(factor(c("a", "a", "b", "b")))
#'
length_unique <- function(x) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  # should this only be for atomics?

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  if (is.factor(x)) {
    length(levels(x))
  } else {
    length(unique(x))
  }
}

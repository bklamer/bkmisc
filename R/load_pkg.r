#' Load R packages
#'
#' Load a vector of R packages. Uninstalled packages will be installed, then loaded.
#'
#' @param pkgs A character vector of R package names.
#' @param install TRUE or FALSE. If TRUE, will install missing R packages.
#'
#' @return Same value as [base::require()]
#'
#' @importFrom utils install.packages
#'
#' @export
#'
#' @examples
#'
#' #----------------------------------------------------------------------------
#' # load_pkg() examples
#' #----------------------------------------------------------------------------
#' library(bkmisc)
#'
#' \dontrun{
#' load_pkg(pkgs = c("dplyr", "data.table"), install = FALSE)
#' }
#'
load_pkg <- function(pkgs, install = TRUE) {
  # Check arguments
  check_character(pkgs)
  check_logical(install)

  # Load packages
  loaded <- vapply(
    X = pkgs,
    FUN = require,
    FUN.VALUE = NA,
    character.only = TRUE,
    quietly = TRUE,
    USE.NAMES = FALSE
  )
  if(any(!loaded) && install){
    missing <- pkgs[!loaded]
    install.packages(missing)
    lapply(missing, require, character.only = TRUE)
  }
}

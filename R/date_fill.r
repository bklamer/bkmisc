#' Fill missing date values in a vector
#'
#' Suppose you have a vector of dates with at least one non-missing value and you
#' know that they either 1) have a consistent sequential time difference from one
#' value to the next, or 2) you have additional information in the form of some
#' numeric date offset. This function will fill in missing dates backwards or
#' forwards. You can either specify sequential differences of a single value, or
#' use a vector of differences to apply to each position.
#'
#' @param date A vector of dates.
#' @param diff A numeric vector which represents the time differences to apply.
#' Needs to be length 1 (sequential filling by this time difference value) or
#' `length(date)` (the time difference at position `n` from `n-1`).
#' @param units A string for `diff` units.
#'
#' @return Vector of dates
#'
#' @export
#'
#' @examples
#'
#' #----------------------------------------------------------------------------
#' # date_fill() examples
#' #----------------------------------------------------------------------------
#' library(bkmisc)
#'
#' date <- as.Date(c(NA, NA, "2022-10-05", NA, NA, NA))
#' diff <-  c(1, 1, 1, 3, 1, 1)
#' date
#' date_fill(date, diff)
#'
date_fill <- function(date, diff, units = "days") {
  #-----------------------------------------------------------------------------
  # Check arguments
  #---------------------------------------f--------------------------------------
  check_date(date)
  check_numeric(diff)
  check_string(units)

  #-----------------------------------------------------------------------------
  # Fill in dates
  #-----------------------------------------------------------------------------
  # Position of first and last nonmissing dates
  idx_nonmiss <- which(!is.na(date))
  idx_nonmiss_first <- idx_nonmiss[1]
  idx_nonmiss_last <- idx_nonmiss[length(idx_nonmiss)]

  # Fill in first date
  if(is.na(date[1])) {
    sum_diff = ifelse(
      length(diff) > 1,
      sum(diff[seq_len(idx_nonmiss_first)][-1]),
      sum(diff * (idx_nonmiss_first - 1))
    )
    date[1] <- date[idx_nonmiss_first] + structure(-sum_diff, class = "difftime", units = units)
  }

  # Fill in last date
  if(is.na(date[length(date)])) {
    sum_diff = ifelse(
      length(diff) > 1,
      sum(diff[seq(from = idx_nonmiss_last, to = length(date), by = 1)][-1]),
      sum(diff * (length(date) - idx_nonmiss_last))
    )
    date[length(date)] <- date[idx_nonmiss_last] + structure(sum_diff, class = "difftime", units = units)
  }

  # Fill in all others
  while(anyNA(date) & !all(is.na(date))) {
    idx_miss <- which(is.na(date))
    date_middle <- data.frame(date = date, diff = diff) |>
      dplyr::rowwise() |>
      dplyr::mutate(
        date_middle = date + structure(-diff, class = "difftime", units = units)
      )

    date[idx_miss] <- date_middle$date_middle[idx_miss+1]
  }

  # Return
  date
}

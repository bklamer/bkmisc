#' Fast data.frame lengths
#'
#' The fastest methods to get data.frame column and row lengths.
#'
#' @param x A data frame.
#'
#' @return \code{data.frame}
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # examples
#' #----------------------------------------------------------------------------
#'
#' @name df_lengths
NULL

#' @export
#' @rdname df_lengths
nrow_df <- function(x) {
  .row_names_info(x, 2L)
}

#' @export
#' @rdname df_lengths
ncol_df <- function(x) {
  length(x)
}

#' Progression Free Survival Time
#'
#' A function to calculate the time from start of survival to time of progression,
#' AKA progression free survival time. Progression free survival is generally defined
#' as the the time from start of survival to progression OR death from any cause,
#' with censoring of subjects who are lost to follow-up.
#'
#' @param start A date vector. The start of survival time.
#' @param progression A date vector. The date of progression with `NA` for those who did not progress.
#' @param stop A date vector. The time of last follow-up (time of death or last follow-up time).
#' @param units A character string of "seconds", "minutes", "hours", "days", "weeks", "months", "years".
#'
#' @return numeric vector
#'
#' @export
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # time_pfs() examples.
#' #----------------------------------------------------------------------------
#' library(bkmisc)
#'
time_pfs <- function(start, progression, stop, units = "weeks") {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  check_date(start)
  check_date(progression)
  check_date(stop)
  check_string(units)
  if(!(units %in% c("seconds", "minutes", "hours", "days", "weeks", "months", "years"))) {
    stop(
      "Check the 'units' argument in function bkmisc::time_pfs(). Is it spelled correctly? Must be 'seconds', 'minutes', 'hours', 'days', 'weeks', 'months', or 'years'.",
      call. = FALSE
    )
  }
  stopifnot(all(start <= progression))
  stopifnot(all(start <= stop))
  stopifnot(all(progression <= stop))

  #-----------------------------------------------------------------------------
  # Overwrite stop with progression time
  #-----------------------------------------------------------------------------
  stop[!is.na(progression)] <- progression[!is.na(progression)]

  #-----------------------------------------------------------------------------
  # Return time
  #-----------------------------------------------------------------------------
  time(start = start, stop = stop, units = units)
 }

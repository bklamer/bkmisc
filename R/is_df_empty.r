#' Is data frame empty
#'
#' This function will return TRUE if NULL or the data frame has 0 rows.
#'
#' @param data A data frame.
#'
#' @return TRUE or FALSE
#'
#' @export
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # is_df_empty() examples
#' #----------------------------------------------------------------------------
#' library(bkmisc)
#'
#' d1 <- NULL
#' d2 <- data.frame(NULL)
#' d3 <- data.frame(numeric(0))
#' d4 <- data.frame(a = 1)
#' is_df_empty(d1)
#' is_df_empty(d2)
#' is_df_empty(d3)
#' is_df_empty(d4)
#'
is_df_empty <- function(data) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  # Need to return TRUE before error from checking arguments.
  if(is.null(data)) {
    return(TRUE)
  }
  check_data_frame(data)

  #-----------------------------------------------------------------------------
  # Check for zero rows and return
  #-----------------------------------------------------------------------------
  .row_names_info(data, 2L) == 0
}

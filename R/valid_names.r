#' Makes syntactically valid column names
#'
#' Makes syntactically valid column names out of character vectors. Replaces
#' invalid characters with underscores.
#'
#' @param x A character vector.
#' @param unique `TRUE` or `FALSE`. If `TRUE`, it returns unique names for the case
#' of duplicates.
#' @param lowercase `TRUE` or `FALSE`. If `TRUE`, it converts text to lower case.
#' @param words A named character vector or `NULL`. Use this to replace invalid
#' characters with selected values. The names are the symbols to replace
#' (for example: `@`, `$`, `%`, `&`, etc.), and the values are their words.
#' Default value of `NULL` removes invalid characters or converts them to
#' underscores.
#' @param n_rounds An integer for the number of rounds needed to fix duplicates.
#'
#' @return character vector.
#'
#' @seealso \code{\link{make.names}}
#'
#' @importFrom stats setNames
#'
#' @export
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # valid_names() examples.
#' #----------------------------------------------------------------------------
#' library(bkmisc)
#'
#' d <- data.frame(
#'   `BaD nAmE_/*iS#^baD  ` = c(1, 2, 3),
#'   good_name_is_good = c(4, 5, 6),
#'   check.names = FALSE
#' )
#' names(d)
#' valid_names(names(d))
#' names(d) <- valid_names(names(d))
#' names(d)
#'
#' d <- data.frame(
#'   `BaD nAmE_/*iS#^baD  ` = c(1, 2, 3),
#'   good_name_is_good = c(4, 5, 6),
#'   check.names = FALSE
#' )
#' valid_names(
#'   x = names(d),
#'   words = setNames(
#'     c("_at_", "_dollars_", "_percent_", "_and_", "_per_", "_geq_", "_geq_", "_greaterthan_", "_leq_", "_leq_", "_lessthan_"),
#'     c("@", "$", "%", "&", "/", "≥", ">=", ">", "≤", "<=", "<")
#'   )
#' )
#'
valid_names <- function(
  x,
  unique = TRUE,
  lowercase = TRUE,
  words = NULL,
  n_rounds = 2
) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  check_character(x)
  check_logical(unique)
  check_logical(lowercase)
  if(!is.null(words)){
    check_named_character(words)
  }
  check_numeric(n_rounds)

  #-----------------------------------------------------------------------------
  # Make names valid
  #-----------------------------------------------------------------------------
  # 1. Remove bad characters that don't play nice
  x <- gsub("[[:cntrl:]]+", "", x) # remove 1+ control characters
  x <- iconv(x, "", "ASCII", sub = "") # remove all non-ascii characters
  # 2. Remove characters that should not be converted to underscores and remove
  #    beginning values that make.names() will convert to "X."
  x <- gsub("'|\"", "", x) # remove all quotes
  x <- gsub("^[[:punct:][:blank:]]+", "", x) # remove 1+ unwanted beginners
  # 3. Rename characters with words.
  #    Note: ">=" will match on ">" if ">" is placed before ">=" in argument 'words'.
  for(i in names(words)) {
    x <- gsub(i, words[[i]], x, fixed = TRUE)
  }
  # 4. Run built in make.names(), then fix the resulting dots. Since it's possible
  #    the Nth run will create duplicates, or shift numbers to the beginning,
  #    it needs to be run N+1 times.
  for(i in seq_len(n_rounds)) {
    x <- make.names(x, unique = unique)
    x <- gsub("[.]+", "_", x) # replace 1+ dots with 1 underscore
    x <- gsub("[_]+", "_", x) # replace 1+ underscores with 1 underscore
    x <- gsub("^[_]|[_]$", "", x) # remove beginning and trailing underscores

    warn <- unique && any(duplicated.default(x))
  }
  # 5. Warn if duplicate names are still there
  if(unique && warn) {
    duplicates <- x[duplicated.default(x)]
    warning(
      "bkmisc::valid_names() did not remove all duplicate values: ",
      pretty_print(duplicates),
      "! Try increasing the 'n_rounds' argument."
    )
  }
  # 6. Finally, set to lowercase.
  if(lowercase) {
    x <- tolower(x)
  }

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  x
}

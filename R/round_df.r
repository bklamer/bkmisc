#' Round all numeric columns in a dataframe or matrix
#'
#' This function will round all numeric columns in a data frame or matrix to
#' the specified number of decimal places.
#'
#' @param data A data frame or matrix.
#' @param digits An integer of the number of decimal places.
#'
#' @return data.frame or matrix
#'
#' @importFrom bkdat as_df
#'
#' @export
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # round_df() examples.
#' #----------------------------------------------------------------------------
#' library(bkmisc)
#'
#' d_int <- data.frame(
#' var1 = 1:3,
#' var2 = 4:6,
#' var3 = 6:8
#' )
#' d_num <- data.frame(
#' var1 = c(1.0001, 2, 3),
#' var2 = c(4, 5, 6.1234567),
#' var3 = c(4, 5, 6)
#' )
#' d_mix <- bkdat::as_df(
#' list(
#'   POSIXct = as.POSIXct(c("2017-08-13", "2017-08-14")),
#'   POSIXlt = as.POSIXlt(c("2017-08-13", "2017-08-14")),
#'   Date = as.Date(c("2017-08-13", "2017-08-14")),
#'   numeric = c(1.399, 2.599),
#'   integer = 3:4,
#'   character = letters[1:2],
#'   missing = rep(c(1, NA)),
#'   infinte = rep(c(2, Inf)),
#'   logical = rep(c(TRUE, FALSE)),
#'   factor = factor(letters[1:2]),
#'   list = replicate(2, list(a=data.frame(b = c(1.33, 2))))
#' )
#' )
#' lapply(round_df(data = d_int, digits = 1), class)
#' lapply(round_df(data = d_num, digits = 1), class)
#' lapply(round_df(data = d_mix, digits = 1), class)
#'
round_df <- function(data, digits = 2) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  check_data_frame_or_matrix(data)
  check_numeric(digits)

  #-----------------------------------------------------------------------------
  # Round the data
  #-----------------------------------------------------------------------------
  if(inherits(data, "matrix")) {
    if(!is.double(data)) {
      warning(
        "Check the 'data' argument in bkmisc::round_df(). 'data' has type '",
        paste0(typeof(data)),
        "', so round_df() cannot be used. The original matrix, 'data', was returned."
      )
      return(data)
    }
    index <- apply(data, 2, function(x) {inherits(x, "numeric")})
  } else {
    index <- sapply(data, function(x) {inherits(x, "numeric")})
  }
  data[, index] <- round(data[, index], digits = digits)

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  data
}

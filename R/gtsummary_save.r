#' Save gtsummary table
#'
#' Save a `gtsummary` table object by converting to `flextable` or `huxtable`
#' and then exporting as `docx`, `html`, `xlsx`, `pdf`, or `tex`.
#'
#' @param x A `gtsummary` table object.
#' @param path Filename of exported table.
#' @param method Function used to convert gtsummary object. `gtsummary::as_flex_table` or `gtsummary::as_hux_table`.
#' @param fontsize Passed to flextable::fontsize().
#' @param fontname Passed to flextable::font().
#'
#' @return null
#'
#' @export
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # gtsummary_save() examples.
#' #----------------------------------------------------------------------------
#' library(bkmisc)
#'
#' tbl <- gtsummary::tbl_summary(mtcars)
#' path <- tempfile(fileext = ".docx")
#' gtsummary_save(x = tbl, path = path)
#'
gtsummary_save <- function(
    x,
    path,
    method = gtsummary::as_flex_table,
    fontsize = 10,
    fontname = "Calibri"
) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  check_gtsummary(x)
  check_string(path)
  check_function(method)
  check_numeric(fontsize)
  check_string(fontname)

  ext <- tools::file_ext(path)

  #-----------------------------------------------------------------------------
  # Write tables
  #-----------------------------------------------------------------------------
  tbl <- method(x)
  if(inherits(tbl, "flextable")) {
    if(ext == "docx") {
      tbl |>
        flextable::set_table_properties(width = .4, layout = "autofit") |>
        flextable::line_spacing(space = 1, part = "all") |>
        flextable::padding(padding.top = 0, padding.bottom = 0, part = "all") |>
        flextable::fontsize(size = fontsize, part = "all") |>
        flextable::font(fontname = fontname, part = "all") |>
        flextable::save_as_docx(path = path)
    }
    if(ext == "html") {
      tbl |>
        flextable::save_as_html(path = path)
    }
  }
  if(inherits(tbl, "huxtable")) {
    if(ext == "docx") {
      tbl |>
        huxtable::quick_docx(file = path)
    }
    if(ext == "html") {
      tbl |>
        huxtable::quick_html(file = path)
    }
    if(ext == "xlsx") {
      tbl |>
        huxtable::quick_xlsx(file = path)
    }
    if(ext == "tex") {
      tbl |>
        huxtable::quick_latex(file = path)
    }
    if(ext == "pdf") {
      tbl |>
        huxtable::quick_pdf(file = path)
    }
  }
 }

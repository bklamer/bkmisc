#' Read large files by chunks
#'
#' Have you ever needed to read a file larger than RAM? Have you ever needed
#' only a subset of the data within that large file? I have just the function for you!
#' Meet `bkmisc::read_chunk()`, it uses `data.table::fread()` as the backend file
#' reader and allows you to pass a function to process each data chunk as it's
#' being read in.
#'
#' Warning: If the file has column names, be sure to set `header = TRUE`. If no names, be sure to set
#' `header = FALSE`. Otherwise, rows could be read in twice. Test on example below.
#'
#' @param file String for file path.
#' @param chunk_size Integer for size of chunk (number of rows).
#' @param n_lines Integer for total number of lines to read. Usually the number of lines in the file; as long as the entire file, after processing, can fit into memory.
#' @param fun A function that will be applied to each chunk as they're being read in. The first argument will be passed the chunk data.frame.
#' @param ... Other arguments passed to `fun`.
#' @param sep String for separator between columns. See `?data.table::fread`.
#' @param header TRUE or FALSE. Does the first data line contain column names? See `?data.table::fread`.
#' @param select A character vector of column names or numeric vector of column positions to keep, drops the rest. See `?data.table::fread`.
#' @param col_names A character vector used to name the columns of the data.  See `?data.table::fread` argument `col.names`.
#' @param col_classes A character vector (named \[specific columns\] or unnamed \[all columns\]) of column classes that correspond to columns in the data.  See `?data.table::fread` argument `colClasses`.
#' @param na_strings A character vector of strings which will be used as NA values.  See `?data.table::fread` argument `na.strings`.
#' @param data.table TRUE or FALSE. Return an object of class data.table or not.
#' @param message TRUE or FALSE. If TRUE, will print timing and chunk position as read is progressing.
#'
#' @return data.frame
#'
#' @importFrom data.table fread
#' @importFrom dplyr bind_rows
#'
#' @export
#'
#' @examples
#'
#' #----------------------------------------------------------------------------
#' # read_chunk() examples
#' #----------------------------------------------------------------------------
#' library(bkmisc)
#'
#' # generate fake data
#' n_lines <- 100
#' df <- data.frame(var1 = seq_len(n_lines))
#' # save data to file
#' filepath <- tempfile(pattern = "read_chunk_example", fileext = ".csv")
#' write.csv(x = df, file = filepath, row.names = FALSE)
#' # read data
#' out <- read_chunk(
#'   file = filepath,
#'   chunk_size = 10,
#'   n_lines = n_lines,
#'   fun = function(x) {x[seq_len(nrow(x)) %% 2 == 0, , drop = FALSE]},
#'   header = TRUE
#' )
#' out
#'
read_chunk <- function(
  file,
  chunk_size,
  n_lines = NULL,
  fun = NULL,
  ...,
  sep = "auto",
  header = "auto",
  select = NULL,
  col_names = NULL,
  col_classes = NULL,
  na_strings = c(""),
  data.table = FALSE,
  message = TRUE
) {
  # Get number of lines in file
  if(is.null(n_lines)) {
    if(message) message("Getting number of lines in file - ", Sys.time())

    if(.Platform$OS.type == "unix") {
      n_lines <- as.integer(system2("wc", args = c("-l", file, " | awk '{print $1}'"), stdout = TRUE))
    } else if (.Platform$OS.type == "windows") {
      #system2("powershell", args = c("type", file, " | Measure-Object -line"), stdout = TRUE)
      n_lines <- system2("find", args = c("/v /c \"\" ", file), stdout = TRUE)
      n_lines <- as.integer(sub(".*: ", "", n_lines[2]))
    } else {
      stop("bkmisc::read_chunk() does not know how to find the number of lines of the file on this OS.")
    }
  }

  # Define chunks
  skip <- seq(from = 0, to = n_lines, by = chunk_size)
  n_skip <- length(skip)
  df_chunk <- data.frame(
    skip = skip,
    nmax = rep(chunk_size, n_skip),
    cumsum = cumsum(rep(chunk_size, n_skip))
  )
  if(df_chunk$skip[n_skip] == n_lines) {
    df_chunk <- df_chunk[seq_len(n_skip - 1), ]
    n_skip <- n_skip - 1
  }
  shift_last <- df_chunk$cumsum[n_skip] - n_lines
  df_chunk$nmax[n_skip] <- df_chunk$nmax[n_skip] - shift_last
  df_chunk$cumsum[n_skip] <- df_chunk$cumsum[n_skip] - shift_last

  # get column names if none provided
  if(is.null(col_names)) {
    col_names <- names(fread(file = file, nrows = 0))
  }

  # Pre-allocate storage
  chunk_list <- vector(mode = "list", length = nrow(df_chunk))

  # Read in data
  for(i in seq_along(chunk_list)) {
    if(message) message("Reading chunk ", i, " of ", nrow(df_chunk), " - ", Sys.time())

    df <- fread(
      file = file,
      sep = sep,
      skip = df_chunk$skip[i],
      nrows = df_chunk$nmax[i],
      header = header,
      select = select,
      na.strings = na_strings,
      col.names = col_names,
      colClasses = col_classes,
      data.table = FALSE
    )

    if(message) message("  Processing chunk ", i, " of ", nrow(df_chunk), " - ", Sys.time())

    # Process data
    if(!is.null(fun)) {
      df <- fun(df, ...)
    }

    chunk_list[[i]] <- df

    if(message) message("  Finished chunk ", i, " of ", nrow(df_chunk), " - ", Sys.time())
  }

  # Return data
  if(message) message("Binding rows into a single dataframe - ", Sys.time())
  df <- bind_rows(chunk_list)
  if(message) message("Finished - ", Sys.time())
  df
}

#' Remove NULL and 0-length entries from a list
#'
#' Recursively removes all NULL entries and 0-length entries from a list.
#'
#' @param x A list
#'
#' @return list
#'
#' @export
#'
#' @seealso \code{rlist::\link[rlist]{list.clean}},
#' \code{\link{any_empty_list}}
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # clean_list() examples
#' #----------------------------------------------------------------------------
#' library(bkmisc)
#'
#' l1 <- list(a = 1, NULL, character(0), b = c(1, 2, 3))
#' l2 <- list(
#'   1,
#'   list(
#'     c(1, 2, 3),
#'     list(list(NULL, "a"))
#'   ),
#'   list(
#'     c(1, 2, 3),
#'     list(numeric(0))
#'   ),
#'   c(1, 2, 3)
#' )
#' l3 <- list(a = 1, b = data.frame(a = 1, b = 2))
#'
#' clean_list(l1)
#' clean_list(l2)
#' clean_list(l3)
clean_list <- function(x){
  #-----------------------------------------------------------------------------
  # - Since this function is recursive, just return x if it is not a list.
  # This saves dataframes and other types from being converted to lists
  # by the functions below.
  # - Could also surround the lapply function below with this if statement.
  # - Any other way to do argument checks here???
  #-----------------------------------------------------------------------------
  if(!inherits(x, "list")) {
    return(x)
  }

  #-----------------------------------------------------------------------------
  # Determines if the element should be removed.
  #-----------------------------------------------------------------------------
  is_bad <- function(y) {
    elements <- unlist(y, use.names = FALSE)
    is.null(elements) || (length(elements) == 0)
  }
  x <- Filter(Negate(is_bad), x)

  #-----------------------------------------------------------------------------
  # Recursively remove bad elements.
  # https://stackoverflow.com/q/26539441
  #-----------------------------------------------------------------------------
  x <- lapply(x, function(y) {
    Filter(length, clean_list(y))
  })

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  x
}

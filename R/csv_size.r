#' Size of dataframe saved as CSV
#'
#' Estimate the size of a dataframe saved as a CSV file.
#'
#' @param x A data.frame that would be written to disk as a CSV.
#' @param frac A numeric value between 0 and 1. The fractional size of `x` used for estimation of complete CSV size on disk.
#' @param format TRUE or FALSE. If TRUE, automatically choose nice unit (megabytes, gigabytes, etc) for file size. If FALSE, file size in bytes.
#'
#' @return If `format=FALSE`, a numeric vector. If `format=TRUE`, a character vector.
#'
#' @references <https://stackoverflow.com/a/16847651>
#'
#' @importFrom utils write.csv
#'
#' @export
#'
#' @examples
#'
#' #----------------------------------------------------------------------------
#' # csv_size() examples
#' #----------------------------------------------------------------------------
#' library(bkmisc)
#'
#' # a data.frame with 50 columns and 100,000 rows of all numeric values
#' tmp <- data.frame(replicate(50, rnorm(10^5)))
#' csv_size(x = tmp, frac = 0.1, format = TRUE)
#' csv_size(x = tmp, frac = 0.1, format = FALSE)
#'
csv_size <- function(x, frac = 0.05, format = TRUE) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  check_data_frame_or_matrix(x)
  check_probability(frac)
  check_logical(format)

  #-----------------------------------------------------------------------------
  # Check size
  #-----------------------------------------------------------------------------
  # create temporary file to save csv
  tf <- tempfile()
  # delete csv when finished
  on.exit(unlink(tf))
  # estimate file size from a much smaller sample of data
  n <- ceiling(nrow(x) * frac)
  # save the small data sample to disk
  write.csv(x[seq_len(n), ], file = tf)
  # estimate file size from sample
  size <- 1 / frac * file.info(tf)$size
  # return size
  if(format) {
    utils:::format.object_size(size, units = "auto")
  } else {
    size
  }
}

#' Length not counting NA
#'
#' Length of a vector while excluding NAs.
#'
#' @param x A vector.
#'
#' @return integer
#'
#' @export
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # length_nona() examples
#' #----------------------------------------------------------------------------
#' library(bkmisc)
#'
#' length_nona(c(1, 2, NA))
#'
length_nona <- function(x) sum(!is.na(x))

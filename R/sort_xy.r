#' Sort vector x based on order of vector y
#'
#' Sort vector x based on order of vector y.
#'
#' @param x An atomic vector.
#' @param y An atomic vecotr.
#'
#' @return vector
#'
#' @export
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # sort_xy() examples.
#' #----------------------------------------------------------------------------
#' library(bkmisc)
#'
#' sort_xy(c("z", "a", "b"), letters)
#' sort_xy(3:1, 1:3)
#'
sort_xy <- function(x, y) {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  check_atomic(x)
  check_atomic(y)

  #-----------------------------------------------------------------------------
  # Sort
  #-----------------------------------------------------------------------------
  out <- x[order(match(x, y))]

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  out
}

#' Is object a 1-dimensional atomic vector
#'
#' Returns `TRUE` if vector is 1-dimensional with values of type integer, double,
#' character, logical, or complex.
#'
#' @param x An object.
#'
#' @return TRUE or FALSE
#'
#' @export
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # is_atomic() examples
#' #----------------------------------------------------------------------------
#' library(bkmisc)
#'
#' is_atomic(1)
#' is_atomic(FALSE)
#' is_atomic(matrix(1))
#' is_atomic(c("a", "b"))
#' is_atomic(data.frame(1))
#'
is_atomic <- function(x) {
  ok_types <- c("integer", "double", "character", "logical", "complex")
  x_type <- typeof(x)
  x_dim <- dim(x)

  # Return
  (x_type %in% ok_types) && is.null(x_dim)
}


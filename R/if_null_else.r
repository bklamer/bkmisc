#' Default replacement of NULLs
#'
#' replace `NULL`s with a default value.
#'
#' @param x An object tested for being `NULL`.
#' @param y An object that may replace `x`.
#'
#' @return If `x` is `NULL`, will return `y`; else `x`
#'
#' @export
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # if_null_else() examples
#' #----------------------------------------------------------------------------
#' library(bkmisc)
#'
#' if_null_else(NULL, 1)
#' if_null_else(5, 1)
#'
if_null_else <- function(x, y) {if(is.null(x)) {y} else {x}}

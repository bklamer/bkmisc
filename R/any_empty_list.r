#' Check if a list contains empty elements
#'
#' Recursively checks list elements for NULL or 0-length vectors. Returns TRUE
#' if list contains any NULL or 0-length vectors.
#'
#' @param x A list
#'
#' @return TRUE or FALSE
#'
#' @export
#'
#' @seealso \code{\link{clean_list}}
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # any_empty_list() examples
#' #----------------------------------------------------------------------------
#' library(bkmisc)
#'
#' l1 <- list(a = 1, NULL, character(0), b = c(1, 2, 3))
#' l2 <- list(
#'   a = 1,
#'   list(
#'     b = c(1, 2, 3),
#'     list(list(NULL))
#'   ),
#'   list(
#'     c = c(1, 2, 3),
#'     list(numeric(0))
#'   ),
#'   d = c(1, 2, 3)
#' )
#' l3 <- list(a = 1, b = 2)
#'
#' any_empty_list(l1)
#' any_empty_list(l2)
#' any_empty_list(l3)
#'
any_empty_list <- function(x){
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  check_list(x)

  #-----------------------------------------------------------------------------
  # Return
  # This isn't a good solution, but it works...
  #-----------------------------------------------------------------------------
  !identical(x, clean_list(x))
}

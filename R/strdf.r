#' Compactly Display the Structure of a data.frame
#'
#' Compactly Display the Structure of a data.frame. A better version of \code{\link[utils]{str}}.
#'
#' @param x A data.frame.
#' @param unique_width An integer for the maximum width of `'unique'` string output.
#' @param unique_sort `TRUE` or `FALSE`. If `TRUE`, will sort unique values A to Z
#' or smallest to largest. If `FALSE` will show uniques in order of first occurrence.
#' @param glance_width An integer for the maximum width of `'glance'` string output.
#' @param print `TRUE` or `FALSE`. If `TRUE`, output will be printed to console
#' with right-alignment. If `FALSE` output will be returned as a data.frame.
#'
#' @return data.frame
#'
#' @seealso \code{\link[utils]{str}}
#'
#' @importFrom stringr str_trunc
#' @importFrom utils head
#'
#' @export
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # strdf() examples
#' #----------------------------------------------------------------------------
#' library(bkmisc)
#'
#' str(mtcars)
#' strdf(mtcars)
#'
strdf <- function(x, unique_width = 20, unique_sort = FALSE, glance_width = 20, print = TRUE) {
  check_data_frame(x)
  check_numeric(unique_width)
  check_logical(unique_sort)
  check_numeric(glance_width)
  check_logical(print)

  name <- names(x)
  n <- vapply(x, function(x) {sum(!is.na(x))}, integer(1))
  n_missing <- vapply(x, function(x) {sum(is.na(x))}, integer(1))
  type <- vapply(x, function(x) {str_trunc(paste(class(x), collapse = ", "), glance_width)}, character(1))
  n_unique <- vapply(x, function(x) {length(unique(x))}, integer(1))
  sort2 <- if(unique_sort) {sort} else (function(x) {x})
  unique <- vapply(x, function(x) {str_trunc(paste(head(sort2(unique(x)), 15), collapse = ", "), glance_width)}, character(1))
  glance <- vapply(x, function(x) {str_trunc(paste(head(x, 15), collapse = ", "), glance_width)}, character(1))

  out <- data.frame(
    name = name,
    type = type,
    n = n,
    n_missing = n_missing,
    n_unique = n_unique,
    unique = unique,
    glance = glance,
    row.names = NULL
  )

  if(print) {
    print(out, right = FALSE)
  } else {
    out
  }
}

#' Simple vector type check
#'
#' Returns type of vector (character, factor, date, numeric).
#'
#' @param x A vector.
#'
#' @return A string from c("character", "factor", "date", "numeric", "unknown")
#'
#' @export
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # type() examples.
#' #----------------------------------------------------------------------------
#' library(bkmisc)
#'
#' type(1)
#` type(1L)
#'
type <- function(x) {
  if(is.numeric(x)) {return("numeric")}
  if(is.factor(x)) {return("factor")}
  if(is.character(x)) {return("character")}
  if(is_date(x)) {return("date")}
  return("unknown")
}

type2 <- function(x) {
  if(is.numeric(x)) {return("numeric")}
  if(is.factor(x)) {return("categorical")}
  if(is.character(x)) {return("categorical")}
  if(is_date(x)) {return("date")}
  return("unknown")
}

#' Suppress printed output
#'
#' Suppresses printed output from terribly written functions. Works well for a variety of situations.
#'
#' @param x The expression to be evaluated.
#'
#' @return Hopefully what the original function should have returned in the first place.
#'
#' @seealso \code{\link{suppressMessages}}
#'
#' @export
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # quiet() examples.
#' #----------------------------------------------------------------------------
#' library(bkmisc)
#'
#' bad_fun <- function(x) {
#'   cat("I'm a bad function")
#'   x
#' }
#' out1 <- bad_fun(2)
#' out2 <- quiet(bad_fun(2))
#'
#' out1
#' out2
#'
quiet <- function(x) {
  sink(tempfile())
  on.exit(sink())
  invisible(force(x))
}

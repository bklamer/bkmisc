#' Argument checks
#'
#' Functions for checking arguments in user created functions. Will return a nice
#' error if the argument is missing or is not of the specified type.
#'
#' The following argument types can be checked:
#'
#' | Type            | Check |
#' |-----------------|-------|
#' | data.frame      | inherits(arg, "data.frame") |
#' | group_df        | inherits(arg, "group_df") |
#' | nest_df         | inherits(arg, "nest_df") |
#' | list            | inherits(arg, "list") |
#' | table           | inherits(arg, "table") |
#' | string          | is_string(arg) |
#' | character       | is.character(arg) |
#' | logical         | is.logical(arg) |
#' | integer         | is_integer(arg) |
#' | numeric         | is.numeric(arg) |
#' | probability     | is.numeric(arg) && (all(arg >= 0L) && all(arg <= 1L)) |
#' | positive        | is.numeric(arg) && all(arg >= 0L) |
#' | correlation     | is.numeric(arg) && (all(arg >= -1L) && all(arg <= 1L)) |
#' | factor          | inherits(arg, "factor") |
#' | date            | is_date(arg) |
#' | data.frame or matrix | inherits(arg, "data.frame") || inherits(arg, "matrix") |
#' | character or numeric | is.character(arg) || is.numeric(arg) |
#' | numeric or logical | is.numeric(arg) || is.logical(arg) |
#' | string or numeric | is_string(arg) || is.numeric(arg) |
#' | symbol or call  | is.symbol(arg) || is.call(arg) |
#' | string or symbol | is_string(arg) || is.symbol(arg) |
#' | `function`      | is.function(arg) |
#' | function or numeric | is.function(arg) || is.numeric(arg) |
#' | formula         | inherits(arg, "formula") |
#' | atomic          | is_atomic(arg) |
#' | named character | (is.character(arg) || (inherits(arg, "list") && is.character(unlist(arg)))) && !("" %in% names(arg) || is.null(names(arg))) |
#' | named vector    | (is.atomic(arg) || (inherits(arg, "list"))) && !("" %in% names(arg) || is.null(names(arg))) |
#'
#' @param arg The argument.
#' @param check_missing `TRUE` or `FALSE`.
#' @param level An integer. Controls the number of levels up to look for original function call.
#' @param name A character vector of column names in a data.frame.
#' @param df A data.frame.
#' @param x An atomic vector. Usually a character vector.
#' @param y An atomic vector. Usually a character vector.
#'
#' @return Error or nothing
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # check examples
#' #----------------------------------------------------------------------------
#' \dontrun{
#' f <- function(x) {
#'   check_data_frame(x, check_missing = TRUE)
#' }
#' f(data.frame())
#' f(1)
#' f()
#' }
#'
#' @name check
NULL

# Below I use a lot of repeated code because I don't know how to deparse
# argument names 2 levels up and it makes the function as fast as possible.
#
# An alternative method as a single function could be:
#
# check_arg <- function(arg, type, check_missing = FALSE, level = 2) {
#   if(check_missing) {
#     check_missing(arg, level = level)
#   }
#
#   is_bad <- function(type, arg) {
#     switch(
#       type,
#       data.frame = !inherits(arg, "data.frame"),
#       group_df = !inherits(arg, "group_df"),
#       nest_df = !inherits(arg, "nest_df"),
#       list = !inherits(arg, "list"),
#       table = !inherits(arg, "table"),
#       string = !is_string(arg),
#       character = !is.character(arg),
#       logical = !is.logical(arg),
#       numeric = !is.numeric(arg),
#       probability = !(is.numeric(arg) && (arg >= 0L && arg <= 1L)),
#       positive = !(is.numeric(arg) && (arg >= 0L)),
#       correlation = !(is.numeric(arg) && (arg >= -1L && arg <= 1L)),
#       factor = !inherits(arg, "factor"),
#       date = !is_date(arg),
#       data.frame_or_matrix = !inherits(arg, "data.frame") && !inherits(arg, "matrix"),
#       character_or_numeric = !is.character(arg) && !is.numeric(arg),
#       numeric_or_logical = !is.numeric(arg) && !is.logical(arg),
#       string_or_numeric = !is_string(arg) && !is.numeric(arg),
#       symbol_or_call = !is.symbol(arg) && !is.call(arg),
#       string_or_symbol = !is_string(arg) && !is.symbol(arg),
#       `function` = !is.function(arg),
#       function_or_numeric = !is.function(arg) && !is.numeric(arg),
#       formula = !inherits(arg, "formula"),
#       atomic = !is_atomic(arg),
#       # can be a named character vector or named list of strings
#       named_character = (!is.character(arg) && !(inherits(arg, "list") && is.character(unlist(arg)))) || ("" %in% names(arg) || is.null(names(arg))),
#       named_vector = (!is.atomic(arg) && !(inherits(arg, "list"))) || ("" %in% names(arg) || is.null(names(arg)))
#     )
#   }
#
#   if(is_bad(type, arg)) {
#     arg_name <- deparse(substitute(arg))
#     fun_name <- deparse(sys.calls()[[sys.nframe()-1]])
#     stop(
#       "Check the '",
#       arg_name,
#       "' argument in function: \n\n",
#       fun_name,
#       "\n\n",
#       "'",
#       arg_name,
#       "' has class ",
#       pretty_print(class(arg)),
#       ", but needs to be a ",
#       type,
#       ".",
#       call. = FALSE
#     )
#   }
# }

#---------------check_data_frame------------------------------------------------
#' @export
#' @rdname check
check_data_frame <- function(
    arg,
    check_missing = FALSE,
    level = 1L
) {
  if(check_missing) {
    if(missing(arg)) {
      arg_name <- deparse(substitute(arg))
      fun_name <- deparse(sys.calls()[[sys.nframe() - level]])
      stop(check_missing_message(arg_name, fun_name), call. = FALSE)
    }
  }
  expected = inherits(arg, "data.frame")
  type = "data.frame"
  if(!expected) {
    arg_name <- deparse(substitute(arg))
    fun_name <- deparse(sys.calls()[[sys.nframe() - level]])
    stop(check_type_message(arg_name, fun_name, arg, type), call. = FALSE)
  }
}

#---------------check_group_df--------------------------------------------------
#' @export
#' @rdname check
check_group_df <- function(
    arg,
    check_missing = FALSE,
    level = 1L
) {
  if(check_missing) {
    if(missing(arg)) {
      arg_name <- deparse(substitute(arg))
      fun_name <- deparse(sys.calls()[[sys.nframe() - level]])
      stop(check_missing_message(arg_name, fun_name), call. = FALSE)
    }
  }
  expected = inherits(arg, "group_df")
  type = "group_df"
  if(!expected) {
    arg_name <- deparse(substitute(arg))
    fun_name <- deparse(sys.calls()[[sys.nframe() - level]])
    stop(check_type_message(arg_name, fun_name, arg, type), call. = FALSE)
  }
}

#---------------check_nest_df---------------------------------------------------
#' @export
#' @rdname check
check_nest_df <- function(
    arg,
    check_missing = FALSE,
    level = 1L
) {
  if(check_missing) {
    if(missing(arg)) {
      arg_name <- deparse(substitute(arg))
      fun_name <- deparse(sys.calls()[[sys.nframe() - level]])
      stop(check_missing_message(arg_name, fun_name), call. = FALSE)
    }
  }
  expected = inherits(arg, "nest_df")
  type = "nest_df"
  if(!expected) {
    arg_name <- deparse(substitute(arg))
    fun_name <- deparse(sys.calls()[[sys.nframe() - level]])
    stop(check_type_message(arg_name, fun_name, arg, type), call. = FALSE)
  }
}

#---------------check_list------------------------------------------------------
#' @export
#' @rdname check
check_list <- function(
    arg,
    check_missing = FALSE,
    level = 1L
) {
  if(check_missing) {
    if(missing(arg)) {
      arg_name <- deparse(substitute(arg))
      fun_name <- deparse(sys.calls()[[sys.nframe() - level]])
      stop(check_missing_message(arg_name, fun_name), call. = FALSE)
    }
  }
  expected = inherits(arg, "list")
  type = "list"
  if(!expected) {
    arg_name <- deparse(substitute(arg))
    fun_name <- deparse(sys.calls()[[sys.nframe() - level]])
    stop(check_type_message(arg_name, fun_name, arg, type), call. = FALSE)
  }
}

#---------------check_table-----------------------------------------------------
#' @export
#' @rdname check
check_table <- function(
    arg,
    check_missing = FALSE,
    level = 1L
) {
  if(check_missing) {
    if(missing(arg)) {
      arg_name <- deparse(substitute(arg))
      fun_name <- deparse(sys.calls()[[sys.nframe() - level]])
      stop(check_missing_message(arg_name, fun_name), call. = FALSE)
    }
  }
  expected = inherits(arg, "table")
  type = "table"
  if(!expected) {
    arg_name <- deparse(substitute(arg))
    fun_name <- deparse(sys.calls()[[sys.nframe() - level]])
    stop(check_type_message(arg_name, fun_name, arg, type), call. = FALSE)
  }
}

#---------------check_string----------------------------------------------------
#' @export
#' @rdname check
check_string <- function(
    arg,
    check_missing = FALSE,
    level = 1L
) {
  if(check_missing) {
    if(missing(arg)) {
      arg_name <- deparse(substitute(arg))
      fun_name <- deparse(sys.calls()[[sys.nframe() - level]])
      stop(check_missing_message(arg_name, fun_name), call. = FALSE)
    }
  }
  expected = is_string(arg)
  type = "string"
  if(!expected) {
    arg_name <- deparse(substitute(arg))
    fun_name <- deparse(sys.calls()[[sys.nframe() - level]])
    stop(check_type_message(arg_name, fun_name, arg, type), call. = FALSE)
  }
}

#---------------check_character-------------------------------------------------
#' @export
#' @rdname check
check_character <- function(
    arg,
    check_missing = FALSE,
    level = 1L
) {
  if(check_missing) {
    if(missing(arg)) {
      arg_name <- deparse(substitute(arg))
      fun_name <- deparse(sys.calls()[[sys.nframe() - level]])
      stop(check_missing_message(arg_name, fun_name), call. = FALSE)
    }
  }
  expected = is.character(arg)
  type = "character"
  if(!expected) {
    arg_name <- deparse(substitute(arg))
    fun_name <- deparse(sys.calls()[[sys.nframe() - level]])
    stop(check_type_message(arg_name, fun_name, arg, type), call. = FALSE)
  }
}

#---------------check_logical---------------------------------------------------
#' @export
#' @rdname check
check_logical <- function(
    arg,
    check_missing = FALSE,
    level = 1L
) {
  if(check_missing) {
    if(missing(arg)) {
      arg_name <- deparse(substitute(arg))
      fun_name <- deparse(sys.calls()[[sys.nframe() - level]])
      stop(check_missing_message(arg_name, fun_name), call. = FALSE)
    }
  }
  expected = is.logical(arg)
  type = "logical"
  if(!expected) {
    arg_name <- deparse(substitute(arg))
    fun_name <- deparse(sys.calls()[[sys.nframe() - level]])
    stop(check_type_message(arg_name, fun_name, arg, type), call. = FALSE)
  }
}

#---------------check_integer---------------------------------------------------
#' @export
#' @rdname check
check_integer <- function(
    arg,
    check_missing = FALSE,
    level = 1L
) {
  if(check_missing) {
    if(missing(arg)) {
      arg_name <- deparse(substitute(arg))
      fun_name <- deparse(sys.calls()[[sys.nframe() - level]])
      stop(check_missing_message(arg_name, fun_name), call. = FALSE)
    }
  }
  expected = is_integer(arg)
  type = "integer"
  if(!expected) {
    arg_name <- deparse(substitute(arg))
    fun_name <- deparse(sys.calls()[[sys.nframe() - level]])
    stop(check_type_message(arg_name, fun_name, arg, type), call. = FALSE)
  }
}

#---------------check_numeric---------------------------------------------------
#' @export
#' @rdname check
check_numeric <- function(
    arg,
    check_missing = FALSE,
    level = 1L
) {
  if(check_missing) {
    if(missing(arg)) {
      arg_name <- deparse(substitute(arg))
      fun_name <- deparse(sys.calls()[[sys.nframe() - level]])
      stop(check_missing_message(arg_name, fun_name), call. = FALSE)
    }
  }
  expected = is.numeric(arg)
  type = "numeric"
  if(!expected) {
    arg_name <- deparse(substitute(arg))
    fun_name <- deparse(sys.calls()[[sys.nframe() - level]])
    stop(check_type_message(arg_name, fun_name, arg, type), call. = FALSE)
  }
}

#---------------check_probability-----------------------------------------------
#' @export
#' @rdname check
check_probability <- function(
    arg,
    check_missing = FALSE,
    level = 1L
) {
  if(check_missing) {
    if(missing(arg)) {
      arg_name <- deparse(substitute(arg))
      fun_name <- deparse(sys.calls()[[sys.nframe() - level]])
      stop(check_missing_message(arg_name, fun_name), call. = FALSE)
    }
  }
  expected = is.numeric(arg) && (all(arg >= 0L) && all(arg <= 1L))
  type = "probability"
  if(!expected) {
    arg_name <- deparse(substitute(arg))
    fun_name <- deparse(sys.calls()[[sys.nframe() - level]])
    stop(check_type_message(arg_name, fun_name, arg, type), call. = FALSE)
  }
}

#---------------check_positive--------------------------------------------------
#' @export
#' @rdname check
check_positive <- function(
    arg,
    check_missing = FALSE,
    level = 1L
) {
  if(check_missing) {
    if(missing(arg)) {
      arg_name <- deparse(substitute(arg))
      fun_name <- deparse(sys.calls()[[sys.nframe() - level]])
      stop(check_missing_message(arg_name, fun_name), call. = FALSE)
    }
  }
  expected = is.numeric(arg) && all(arg >= 0L)
  type = "positive number"
  if(!expected) {
    arg_name <- deparse(substitute(arg))
    fun_name <- deparse(sys.calls()[[sys.nframe() - level]])
    stop(check_type_message(arg_name, fun_name, arg, type), call. = FALSE)
  }
}

#---------------check_correlation-----------------------------------------------
#' @export
#' @rdname check
check_correlation <- function(
    arg,
    check_missing = FALSE,
    level = 1L
) {
  if(check_missing) {
    if(missing(arg)) {
      arg_name <- deparse(substitute(arg))
      fun_name <- deparse(sys.calls()[[sys.nframe() - level]])
      stop(check_missing_message(arg_name, fun_name), call. = FALSE)
    }
  }
  expected = is.numeric(arg) && (all(arg >= -1L) && all(arg <= 1L))
  type = "correlation"
  if(!expected) {
    arg_name <- deparse(substitute(arg))
    fun_name <- deparse(sys.calls()[[sys.nframe() - level]])
    stop(check_type_message(arg_name, fun_name, arg, type), call. = FALSE)
  }
}

#---------------check_factor----------------------------------------------------
#' @export
#' @rdname check
check_factor <- function(
    arg,
    check_missing = FALSE,
    level = 1L
) {
  if(check_missing) {
    if(missing(arg)) {
      arg_name <- deparse(substitute(arg))
      fun_name <- deparse(sys.calls()[[sys.nframe() - level]])
      stop(check_missing_message(arg_name, fun_name), call. = FALSE)
    }
  }
  expected = inherits(arg, "factor")
  type = "factor"
  if(!expected) {
    arg_name <- deparse(substitute(arg))
    fun_name <- deparse(sys.calls()[[sys.nframe() - level]])
    stop(check_type_message(arg_name, fun_name, arg, type), call. = FALSE)
  }
}

#---------------check_date------------------------------------------------------
#' @export
#' @rdname check
check_date <- function(
    arg,
    check_missing = FALSE,
    level = 1L
) {
  if(check_missing) {
    if(missing(arg)) {
      arg_name <- deparse(substitute(arg))
      fun_name <- deparse(sys.calls()[[sys.nframe() - level]])
      stop(check_missing_message(arg_name, fun_name), call. = FALSE)
    }
  }
  expected = is_date(arg)
  type = "date"
  if(!expected) {
    arg_name <- deparse(substitute(arg))
    fun_name <- deparse(sys.calls()[[sys.nframe() - level]])
    stop(check_type_message(arg_name, fun_name, arg, type), call. = FALSE)
  }
}

#---------------check_data_frame_or_matrix--------------------------------------
#' @export
#' @rdname check
check_data_frame_or_matrix <- function(
    arg,
    check_missing = FALSE,
    level = 1L
) {
  if(check_missing) {
    if(missing(arg)) {
      arg_name <- deparse(substitute(arg))
      fun_name <- deparse(sys.calls()[[sys.nframe() - level]])
      stop(check_missing_message(arg_name, fun_name), call. = FALSE)
    }
  }
  expected = inherits(arg, "data.frame") || inherits(arg, "matrix")
  type = "data.frame or matrix"
  if(!expected) {
    arg_name <- deparse(substitute(arg))
    fun_name <- deparse(sys.calls()[[sys.nframe() - level]])
    stop(check_type_message(arg_name, fun_name, arg, type), call. = FALSE)
  }
}

#---------------check_character_or_numeric--------------------------------------
#' @export
#' @rdname check
check_character_or_numeric <- function(
    arg,
    check_missing = FALSE,
    level = 1L
) {
  if(check_missing) {
    if(missing(arg)) {
      arg_name <- deparse(substitute(arg))
      fun_name <- deparse(sys.calls()[[sys.nframe() - level]])
      stop(check_missing_message(arg_name, fun_name), call. = FALSE)
    }
  }
  expected = is.character(arg) || is.numeric(arg)
  type = "character or numeric"
  if(!expected) {
    arg_name <- deparse(substitute(arg))
    fun_name <- deparse(sys.calls()[[sys.nframe() - level]])
    stop(check_type_message(arg_name, fun_name, arg, type), call. = FALSE)
  }
}

#---------------check_numeric_or_logical----------------------------------------
#' @export
#' @rdname check
check_numeric_or_logical <- function(
    arg,
    check_missing = FALSE,
    level = 1L
) {
  if(check_missing) {
    if(missing(arg)) {
      arg_name <- deparse(substitute(arg))
      fun_name <- deparse(sys.calls()[[sys.nframe() - level]])
      stop(check_missing_message(arg_name, fun_name), call. = FALSE)
    }
  }
  expected = is.numeric(arg) || is.logical(arg)
  type = "number or logical"
  if(!expected) {
    arg_name <- deparse(substitute(arg))
    fun_name <- deparse(sys.calls()[[sys.nframe() - level]])
    stop(check_type_message(arg_name, fun_name, arg, type), call. = FALSE)
  }
}

#---------------check_string_or_numeric-----------------------------------------
#' @export
#' @rdname check
check_string_or_numeric <- function(
    arg,
    check_missing = FALSE,
    level = 1L
) {
  if(check_missing) {
    if(missing(arg)) {
      arg_name <- deparse(substitute(arg))
      fun_name <- deparse(sys.calls()[[sys.nframe() - level]])
      stop(check_missing_message(arg_name, fun_name), call. = FALSE)
    }
  }
  expected = is_string(arg) || is.numeric(arg)
  type = "string or number"
  if(!expected) {
    arg_name <- deparse(substitute(arg))
    fun_name <- deparse(sys.calls()[[sys.nframe() - level]])
    stop(check_type_message(arg_name, fun_name, arg, type), call. = FALSE)
  }
}

#---------------check_symbol_or_call--------------------------------------------
#' @export
#' @rdname check
check_symbol_or_call <- function(
    arg,
    check_missing = FALSE,
    level = 1L
) {
  if(check_missing) {
    if(missing(arg)) {
      arg_name <- deparse(substitute(arg))
      fun_name <- deparse(sys.calls()[[sys.nframe() - level]])
      stop(check_missing_message(arg_name, fun_name), call. = FALSE)
    }
  }
  expected = is.symbol(arg) || is.call(arg)
  type = "symbol or call"
  if(!expected) {
    arg_name <- deparse(substitute(arg))
    fun_name <- deparse(sys.calls()[[sys.nframe() - level]])
    stop(check_type_message(arg_name, fun_name, arg, type), call. = FALSE)
  }
}

#---------------check_string_or_symbol------------------------------------------
#' @export
#' @rdname check
check_string_or_symbol <- function(
    arg,
    check_missing = FALSE,
    level = 1L
) {
  if(check_missing) {
    if(missing(arg)) {
      arg_name <- deparse(substitute(arg))
      fun_name <- deparse(sys.calls()[[sys.nframe() - level]])
      stop(check_missing_message(arg_name, fun_name), call. = FALSE)
    }
  }
  expected = is_string(arg) || is.symbol(arg)
  type = "string or symbol"
  if(!expected) {
    arg_name <- deparse(substitute(arg))
    fun_name <- deparse(sys.calls()[[sys.nframe() - level]])
    stop(check_type_message(arg_name, fun_name, arg, type), call. = FALSE)
  }
}

#---------------check_function--------------------------------------------------
#' @export
#' @rdname check
check_function <- function(
    arg,
    check_missing = FALSE,
    level = 1L
) {
  if(check_missing) {
    if(missing(arg)) {
      arg_name <- deparse(substitute(arg))
      fun_name <- deparse(sys.calls()[[sys.nframe() - level]])
      stop(check_missing_message(arg_name, fun_name), call. = FALSE)
    }
  }
  expected = is.function(arg)
  type = "function"
  if(!expected) {
    arg_name <- deparse(substitute(arg))
    fun_name <- deparse(sys.calls()[[sys.nframe() - level]])
    stop(check_type_message(arg_name, fun_name, arg, type), call. = FALSE)
  }
}

#---------------check_function_or_numeric---------------------------------------
#' @export
#' @rdname check
check_function_or_numeric <- function(
    arg,
    check_missing = FALSE,
    level = 1L
) {
  if(check_missing) {
    if(missing(arg)) {
      arg_name <- deparse(substitute(arg))
      fun_name <- deparse(sys.calls()[[sys.nframe() - level]])
      stop(check_missing_message(arg_name, fun_name), call. = FALSE)
    }
  }
  expected = is.function(arg) || is.numeric(arg)
  type = "function or numeric"
  if(!expected) {
    arg_name <- deparse(substitute(arg))
    fun_name <- deparse(sys.calls()[[sys.nframe() - level]])
    stop(check_type_message(arg_name, fun_name, arg, type), call. = FALSE)
  }
}

#---------------check_formula---------------------------------------------------
#' @export
#' @rdname check
check_formula <- function(
    arg,
    check_missing = FALSE,
    level = 1L
) {
  if(check_missing) {
    if(missing(arg)) {
      arg_name <- deparse(substitute(arg))
      fun_name <- deparse(sys.calls()[[sys.nframe() - level]])
      stop(check_missing_message(arg_name, fun_name), call. = FALSE)
    }
  }
  expected = inherits(arg, "formula")
  type = "formula"
  if(!expected) {
    arg_name <- deparse(substitute(arg))
    fun_name <- deparse(sys.calls()[[sys.nframe() - level]])
    stop(check_type_message(arg_name, fun_name, arg, type), call. = FALSE)
  }
}

#---------------check_atomic----------------------------------------------------
#' @export
#' @rdname check
check_atomic <- function(
    arg,
    check_missing = FALSE,
    level = 1L
) {
  if(check_missing) {
    if(missing(arg)) {
      arg_name <- deparse(substitute(arg))
      fun_name <- deparse(sys.calls()[[sys.nframe() - level]])
      stop(check_missing_message(arg_name, fun_name), call. = FALSE)
    }
  }
  expected = is_atomic(arg)
  type = "atomic"
  if(!expected) {
    arg_name <- deparse(substitute(arg))
    fun_name <- deparse(sys.calls()[[sys.nframe() - level]])
    stop(check_type_message(arg_name, fun_name, arg, type), call. = FALSE)
  }
}

#---------------check_named_character-------------------------------------------
#' @export
#' @rdname check
check_named_character <- function(
    arg,
    check_missing = FALSE,
    level = 1L
) {
  if(check_missing) {
    if(missing(arg)) {
      arg_name <- deparse(substitute(arg))
      fun_name <- deparse(sys.calls()[[sys.nframe() - level]])
      stop(check_missing_message(arg_name, fun_name), call. = FALSE)
    }
  }
  expected = (is.character(arg) || (inherits(arg, "list") && is.character(unlist(arg)))) && !("" %in% names(arg) || is.null(names(arg)))
  type = "named character"
  if(!expected) {
    arg_name <- deparse(substitute(arg))
    fun_name <- deparse(sys.calls()[[sys.nframe() - level]])
    stop(check_type_message(arg_name, fun_name, arg, type), call. = FALSE)
  }
}

#---------------check_named_vector----------------------------------------------
#' @export
#' @rdname check
check_named_vector <- function(
    arg,
    check_missing = FALSE,
    level = 1L
) {
  if(check_missing) {
    if(missing(arg)) {
      arg_name <- deparse(substitute(arg))
      fun_name <- deparse(sys.calls()[[sys.nframe() - level]])
      stop(check_missing_message(arg_name, fun_name), call. = FALSE)
    }
  }
  expected = (is.atomic(arg) || (inherits(arg, "list"))) && !("" %in% names(arg) || is.null(names(arg)))
  type = "named vector"
  if(!expected) {
    arg_name <- deparse(substitute(arg))
    fun_name <- deparse(sys.calls()[[sys.nframe() - level]])
    stop(check_type_message(arg_name, fun_name, arg, type), call. = FALSE)
  }
}

#---------------check_gtsummary----------------------------------------------
#' @export
#' @rdname check
check_gtsummary <- function(
    arg,
    check_missing = FALSE,
    level = 1L
) {
  if(check_missing) {
    if(missing(arg)) {
      arg_name <- deparse(substitute(arg))
      fun_name <- deparse(sys.calls()[[sys.nframe() - level]])
      stop(check_missing_message(arg_name, fun_name), call. = FALSE)
    }
  }
  expected = inherits(arg, "gtsummary")
  type = "gtsummary"
  if(!expected) {
    arg_name <- deparse(substitute(arg))
    fun_name <- deparse(sys.calls()[[sys.nframe() - level]])
    stop(check_type_message(arg_name, fun_name, arg, type), call. = FALSE)
  }
}

#---------------check_missing---------------------------------------------------
#' @export
#' @rdname check
check_missing <- function(arg, level = 1L) {
  if(missing(arg)) {
    arg_name <- deparse(substitute(arg))
    fun_name <- deparse(sys.calls()[[sys.nframe() - level]])
    stop(check_missing_message(arg_name, fun_name), call. = FALSE)
  }
}

# check_name_in_df() and check_x_in_y() are essentially the same function. However,
# their distinction in error messages and function names is justification to
# create two separate functions.
#' @export
#' @rdname check
check_name_in_df <- function(name, df, level = 1L) {
  check_character(name)
  check_data_frame(df)
  any_missing <- !all(name %in% names(df))
  if(any_missing) {
    arg_name <- deparse(substitute(name))
    fun_name <- deparse(sys.calls()[[sys.nframe() - level]])
    are_missing <- !(name %in% names(df))
    stop(
      check_name_in_df_message(arg_name, fun_name, df, name, are_missing),
      call. = FALSE
    )
  }
}

#' @export
#' @rdname check
check_x_in_y <- function(x, y, level = 1L) {
  any_missing <- !all(x %in% y)
  if(any_missing) {
    arg_name <- deparse(substitute(x))
    fun_name <- deparse(sys.calls()[[sys.nframe() - level]])
    are_missing <- !(x %in% y)
    stop(
      check_x_in_y_message(arg_name, fun_name, y, x, are_missing),
      call. = FALSE
    )
  }
}

check_type_message <- function(arg_name, fun_name, arg, type) {
  paste0(
    "Check the '", arg_name, "' argument in function: \n\n", fun_name,
    "\n\n", "'", arg_name, "' has class ", pretty_print(class(arg)),
    ", but needs to be a ", type, "."
  )
}

check_missing_message <- function(arg_name, fun_name) {
  paste0(
    "Check the '", arg_name, "' argument in function: \n\n", fun_name,
    "\n\n", "'", arg_name, "' is missing."
  )
}

check_name_in_df_message <- function(arg_name, fun_name, df, name, are_missing) {
  paste0(
    "Check the '", arg_name, "' argument in function: \n\n", fun_name,
    "\n\n", "The following names were not found in '", deparse(substitute(df)),
    "': ", pretty_print(name[are_missing]), "."
  )
}

check_x_in_y_message <- function(arg_name, fun_name, y, x, are_missing) {
  paste0(
    "Check the '", arg_name, "' argument in function: \n\n", fun_name,
    "\n\n", "The following values were not found in '", deparse(substitute(y)),
    "': ", pretty_print(x[are_missing]), "."
  )
}

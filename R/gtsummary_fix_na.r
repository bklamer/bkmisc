#' Better `NA` handling in gtsummary
#'
#' Provides better `NA` handling in [gtsummary::tbl_summary()].
#'
#' @param table A [gtsummary::tbl_summary()] object.
#'
#' @return gtsummary
#'
#' @importFrom dplyr mutate across
#' @importFrom tidyselect starts_with
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # gtsummary_fix_na() examples
#' #----------------------------------------------------------------------------
#' library(bkmisc)
#'
#' @export
gtsummary_fix_na <- function(table) {
  table$table_body <- table$table_body |>
    mutate(
      across(
        .cols = starts_with("stat_"),
        .fns = function(x) {ifelse(x == "0 (NA%)", "0", x)}
      )
    ) |>
    mutate(
      across(
        .cols = starts_with("stat_"),
        .fns = function(x) {ifelse(x == "NA (NA, NA)", "NA", x)}
      )
    )

  table
}

#' Time between dates
#'
#' Calculates time between two date vectors.
#'
#' @param start A date vector.
#' @param stop A date vector.
#' @param units A character string of "seconds", "minutes", "hours", "days", "weeks", "months", "years".
#'
#' @return numeric vector
#'
#' @export
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # time() examples.
#' #----------------------------------------------------------------------------
#' library(bkmisc)
#'
#' time1 <- as.Date(c("1992-02-27", "1992-02-27", "1992-01-14", "1992-02-28", "1992-02-01"))
#' time2 <- as.Date(c("1992-02-28", "1992-03-27", "1992-07-14", "1992-04-15", "1993-02-01"))
#' time3 <- as.POSIXct(c("1992-02-27", "1992-02-27", "1992-01-14", "1992-02-28", "1992-02-01"))
#' time4 <- as.POSIXct(c("1992-02-28", "1992-03-27", "1992-07-14", "1992-04-15", "1993-02-01"))
#'
#' time(start = time1, stop = time2, units = "days")
#' time(start = time3, stop = time4, units = "months")
#' time(start = time1, stop = time4, units = "years")
#'
time <- function(start, stop, units = "weeks") {
  #-----------------------------------------------------------------------------
  # Check arguments
  #-----------------------------------------------------------------------------
  check_date(start)
  check_date(stop)
  check_string(units)
  if(!(units %in% c("seconds", "minutes", "hours", "days", "weeks", "months", "years"))) {
    stop(
      "Check the 'units' argument in function bkmisc::time(). Is it spelled correctly? Must be 'seconds', 'minutes', 'hours', 'days', 'weeks', 'months', or 'years'.",
      call. = FALSE
    )
  }

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  if(units %in% c("seconds", "minutes", "hours", "days", "weeks")) {
    ret <- difftime(time1 = stop, time2 = start, units = substr(units, start = 1, stop = 3))
    as.numeric(ret)
  } else if (units == "months") {
    ret <- difftime(time1 = stop, time2 = start, units = "days")
    as.numeric(ret) / (365.25/12)
  } else if (units == "years") {
    ret <- difftime(time1 = stop, time2 = start, units = "days")
    as.numeric(ret) / 365.25
  }
 }

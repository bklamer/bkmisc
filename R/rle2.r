#' Run length encoding
#'
#' Returns the lengths and values of runs of equal values in a vector.
#'
#' [base::rle] and [base::inverse.rle] are nice, but `base::rle` has a return
#' value that is hard to work with. `rle2` returns what you would expect, in a
#' easy to handle dataframe.
#'
#' @param x An atomic vector.
#' @param order `TRUE` or `FALSE`. If `TRUE`, will order data based on the values.
#' @param index `TRUE` or `FALSE`. If `TRUE`, will add the index position for each run/consecutive value in columns `'start'` and `'stop'`.
#'
#' @return data.frame with columns `'value'`, `'length'` and optionally `'start'` and `'stop'`.
#'
#' @importFrom bkdat reset_row_names
#'
#' @export
#'
#' @examples
#'
#' #----------------------------------------------------------------------------
#' # rle2() examples
#' #----------------------------------------------------------------------------
#' library(bkmisc)
#'
#' x <- c(NA, NA, rev(rep(6:10, 1:5)), NA)
#'
#' rle(x)
#' rle2(x)
#'
rle2 <- function(x, order = FALSE, index = TRUE) {
  # Check arguments
  check_atomic(x)
  check_logical(order)
  check_logical(index)

  # Run rle
  ret <- rle_fix(x)

  out <- data.frame(
    value = ret$values,
    length = ret$lengths
  )

  if(index) {
    out$start <- cumsum(out$length) - out$length + 1
    out$stop <- cumsum(out$length)
  }

  if(order) {
    out <- out[order(out$value), ]
    out <- reset_row_names(out)
  }

  # Return
  out
}

# https://coolbutuseless.github.io/2020/08/26/run-length-encoding-and-the-problem-of-nas/
# A replacement of base::rle() that treats all NAs as identical
rle_fix <- function (x)  {
  check_atomic(x)

  n <- length(x)
  if (n == 0L) {
    return(
      structure(
        list(
          lengths = integer(),
          values = x
        )
      ),
      class = 'rle'
    )
  }

  # Where does next value not equal current value?
  # i.e. y will be TRUE at the last index before a change
  y <- (x[-1L] != x[-n])

  # Since NAs are never equal to anything, NAs in 'x' will lead to NAs in 'y'.
  # These current NAs in 'y' tell us nothing - Set all NAs in y to FALSE
  y[is.na(y)] <- FALSE

  # When a value in x is NA and its successor is not (or vice versa) then that
  # should also count as a value change and location in 'y' should be set to TRUE
  y <- y | xor(is.na(x[-1L]), is.na(x[-n]))

  # Any TRUE locations in 'y' are the end of a run, where the next value
  # changes to something different
  i <- c(which(y), n)

  structure(
    list(
      lengths = diff(c(0L, i)),
      values  = x[i]
    ),
    class = 'rle'
  )
}

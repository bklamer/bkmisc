#' Create a named list
#'
#' Creates a list and automatically assigns names using the names of the
#' corresponding objects or, if unnamed, the expressions themselves.
#'
#' @param ... Objects to be stored in a list.
#'
#' @return list
#'
#' @references See page 46 of S Programming by Venables & Ripley
#'
#' @export
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # nlist() examples
#' #----------------------------------------------------------------------------
#' library(bkmisc)
#'
#' a <- 1:5
#'
#' nlist(a, 6:10, x = 3)
#'
nlist <- function(...) {
  #-----------------------------------------------------------------------------
  # Process dots
  #-----------------------------------------------------------------------------
  l <- list(...)
  l_names <- names(dots_char(...))

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  setNames(l, l_names)
}

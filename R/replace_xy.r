#' Replace x to y in data frames or matrices
#'
#' Replace all values of x to value of y in data frames or matrices.
#'
#' \bold{Data frames}
#'
#' Be aware of R's coercion rules when using this function on a data frame of
#' mixed type.
#' Any column that matches the type of '\code{x}' will be a
#' candidate for having it's values replaced with '\code{y}'.
#' This means you will need to use '\code{x = NA_integer_}' to replace NA's in
#' integer columns, '\code{x = NA_real_}' to replace NA's in numeric columns,
#' '\code{x = NA_character_}' to replace NA's in character columns, and
#' '\code{x = NA}' to replace NA's in logical columns.
#'
#' If '\code{y}' is of a different type than '\code{x}', then the candidate
#' column will be coerced to type '\code{y}' or '\code{y}' will be coerced to
#' type '\code{x}'. In general, if '\code{x}' is located in any column on the
#' left of the following list, and '\code{y}' is on the right, then the whole
#' column where '\code{x}' is located will be coerced to the type of
#' '\code{y}'. Similarly, if '\code{x}' is on the right and '\code{y}' is on
#' the left, then '\code{y}' will be coerced to type '\code{x}'.
#'
#' logical -> integer -> numeric -> complex -> character.
#'
#' Some additional notes are in the table below:
#' \tabular{ll}{
#' 	\bold{Column Type}	\tab\bold{Notes}	\cr
#' 	Numeric	\tab Supported	\cr
#' 	Integer	\tab Supported	\cr
#' 	Character	\tab Supported	\cr
#' 	Logical	\tab Supported	\cr
#' 	Factor	\tab  '\code{y}' only replaces '\code{x}' if '\code{y}' is a valid factor level in the factor column.	\cr
#' 	List	\tab A list '\code{y}' only replaces non-list (and not factor) '\code{x}'. Errors for '\code{x}' = 'Inf'.	\cr
#' 	POSIXct	\tab Error	\cr
#' 	POSIXlt	\tab Error	\cr
#' 	Date	\tab Error	\cr
#' }
#'
#' \bold{Matrices}
#'
#' Matrices are always of a single data type. This means type checking is not
#' needed for 'x' (It will select any value in the matrix that a coerced 'x'
#' will match), but types will be checked for 'y'. If the type of 'y' does not
#' match the matrix, then an error will be returned.
#'
#' @param data A data frame or matrix.
#' @param x Value you wish be replaced. A vector of length 1.
#' @param y Value you wish to insert. A vector of length 1.
#'
#' @return data.frame or matrix
#'
#' @importFrom bkdat as_df
#'
#' @export
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # replace_xy() examples.
#' #----------------------------------------------------------------------------
#' library(bkmisc)
#'
#' d <- data.frame(
#' var1 = c(Inf, 2, 3),
#' var2 = c(4, 5, 6)
#' )
#' d
#' replace_xy(data = d, x = Inf, y = NA)
#'
#' m <- matrix(
#' c(NA, 2, 3, 4, 5, 6),
#' nrow = 3,
#' ncol = 2
#' )
#' m
#' replace_xy(data = m, x = NA, y = 0)
#'
replace_xy <- function(data, x, y) {
  UseMethod("replace_xy")
}

#' @export
#' @rdname replace_xy
replace_xy.data.frame <- function(data, x, y) {
  #-----------------------------------------------------------------------------
  # Check the arguments
  #-----------------------------------------------------------------------------
  check_data_frame_or_matrix(data)

  #-----------------------------------------------------------------------------
  # Create list of modified values
  #-----------------------------------------------------------------------------
  # First store the class of what we are replacing.
  x_class <- class(x)
  if(is.na(x)) {
    list <- lapply(data, function(vec) {
      # Need if statement to prevent unwanted coercion
      if(inherits(vec, x_class)) {
        vec[is.na(vec)] <- y # See also the source for replace()
        vec
      } else {
        vec
      }
    })
  } else if(is.infinite(x)) {
    list <- lapply(data, function(vec) {
      if(inherits(vec, x_class)) {
        vec[is.infinite(vec)] <- y
        vec
      } else {
        vec
      }
    })
  } else {
    list <- lapply(data, function(vec) {
      if(inherits(vec, x_class)) {
        is_replaceable <- vec %in% x
        vec[is_replaceable] <- y
        vec
      } else {
        vec
      }
    })
  }

  #-----------------------------------------------------------------------------
  # Convert to data frame and return
  #-----------------------------------------------------------------------------
  as_df(list)
}

#' @export
#' @rdname replace_xy
replace_xy.nest_df <- function(data, x, y) {
  NextMethod()
}

#' @export
#' @rdname replace_xy
replace_xy.group_df <- function(data, x, y) {
  NextMethod()
}

#' @export
#' @rdname replace_xy
replace_xy.matrix <- function(data, x, y) {
  #-----------------------------------------------------------------------------
  # Check the arguments
  #-----------------------------------------------------------------------------
  mat_type <- typeof(data)
  y_type <- typeof(y)
  if(mat_type != y_type) {
    stop(
      "Check the 'y' argument for bkmisc::replace_xy(). The matrix data type is '",
      paste0(mat_type),
      "' and the type of 'y' is '",
      paste0(y_type),
      "'. This would have resulted in coercion. Please change the 'y' argument so it's type matches the matrix type."
    )
  }

  #-----------------------------------------------------------------------------
  # Create matrix of modified values
  #-----------------------------------------------------------------------------
  if(is.na(x)) {
    mat <- apply(data, 2, function(vec) {vec[is.na(vec)] <- y; vec})
  } else if(is.infinite(x)) {
    mat <- apply(data, 2, function(vec) {vec[is.infinite(vec)] <- y; vec})
  } else {
    mat <- apply(data, 2, function(vec) {vec[vec %in% x] <- y; vec})
  }

  #-----------------------------------------------------------------------------
  # Return
  #-----------------------------------------------------------------------------
  mat
}

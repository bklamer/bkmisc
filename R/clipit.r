#' Copy object as code to the clipboard
#'
#' Simple function to `dput()` code to the system clipboard.
#' On Linux systems, 'xclip' (X11) or 'wl-copy' and 'wl-paste' (Wayland) must
#' be installed.
#'
#' @param x An object.
#'
#' @return Invisibly returns the original object
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # examples
#' #----------------------------------------------------------------------------
#' \dontrun{
#' library(bkmisc)
#' x <- factor(letters)
#' clipit(x)
#' }
#'
#' @importFrom clipr write_clip
#' @importFrom utils capture.output
#'
#' @export
clipit <- function(x) {
  # Modified from Jonas K. Lindeløv on twitter
  x |>
    dput() |>
    capture.output() |>
    write_clip()
}

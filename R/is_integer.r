#' Is object an integer vector
#'
#' Returns `TRUE` if vector is an integer.
#'
#' @param x An integer vector.
#'
#' @return TRUE or FALSE
#'
#' @export
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # is_integer() examples
#' #----------------------------------------------------------------------------
#' library(bkmisc)
#'
#' w <- c(1.1, 2L)
#' x <- c(1, 1L, NA)
#' y <- 1
#' z <- 1L
#' is_integer(w)
#' is_integer(x)
#' is_integer(y)
#' is_integer(z)
#'
is_integer <- function(x) {
  # If x=NA, this doesn't ensure strict x=NA_integer_, but I don't see this
  # causing issues.
  x <- x[!is.na(x)]
  all(x == as.integer(x))
}

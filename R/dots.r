#' Capture unevaluated dots
#'
#' Capture unevaluated dots. `dots()` returns list of unevaluated expressions.
#' `dots_char()` returns expressions as a character vector.
#'
#' @param ... Expressions to be captured.
#' @param .check_names `TRUE` or `FALSE`. If `TRUE` will fill in missing names
#' slot using character version of expression.
#'
#' @return
#' - `dots()`: list
#' - `dots_char()`: character vector
#'
#' @seealso \code{pryr::\link[pryr]{dots}}, \code{pryr::\link[pryr]{named_dots}}
#'
#' @export
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # dots() examples
#' #----------------------------------------------------------------------------
#' library(bkmisc)
#'
#' a <- 1:5
#'
#' dots(a, 6:10, x = 3)
#' dots_char(a, 6:10, x = 3)
#'
#' @name dots
NULL

#' @export
#' @rdname dots
dots <- function(..., .check_names = TRUE) {
  # Capture the dots
  dots <- as.list(substitute(list(...)))[-1]
  # Handle cases where names are missing
  if(.check_names) {
    dots_names <- names(dots)
    if(any(dots_names == "") || is.null(dots_names)) {
      are_missing <- if(is.null(dots_names)) {
        rep(TRUE, length(dots))
      } else {
        dots_names == ""
      }
      dots_char <- vapply(
        X = dots,
        FUN = deparse,
        FUN.VALUE = NA_character_,
        USE.NAMES = FALSE
      )
      dots_names[are_missing] <- dots_char[are_missing]
      names(dots) <- dots_names
    }
    # Warning if duplicate names?
    if(any(duplicated.default(dots_names))) {
      warning("The dots argument results in duplicated names!")
    }
  }
  # Return
  dots
}

#' @export
#' @rdname dots
dots_char <- function(..., .check_names = TRUE) {
  # Capture the dots
  dots <- vapply(
    X = as.list(substitute(list(...)))[-1],
    FUN = deparse,
    FUN.VALUE = NA_character_,
    USE.NAMES = TRUE
  )
  # Handle cases where names are missing
  if(.check_names) {
    # If names will be used/matter, we'll process them here
    dots_names <- names(dots)
    if(any(dots_names == "") || is.null(dots_names)) {
      are_missing <- if(is.null(dots_names)) {
        rep(TRUE, length(dots))
      } else {
        dots_names == ""
      }
      dots_names[are_missing] <- dots[are_missing]
      names(dots) <- dots_names
    }
    # Warning if duplicate names?
    if(any(duplicated.default(dots_names))) {
      warning("The dots argument results in duplicated names!")
    }
  }
  # Return
  dots
}

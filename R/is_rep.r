#' Is each element of a vector the same
#'
#' Returns `TRUE` if all elements in a vector are the same.
#'
#' Uses `length(unique(x)) == 1` by default. Uses
#' `abs(max(x, na.rm = TRUE) - min(x, na.rm = TRUE)) < tol` as a faster method
#' for numeric cases when `tol` is set.
#'
#' @param x A vector.
#' @param tol A numeric value for the tolerance factor when comparing numerics.
#' A good value might be `.Machine$double.eps ^ 0.5`. If not NULL, it assumes
#' that `x` is a numeric and `NA` values should be removed.
#'
#' @return `TRUE` or `FALSE`
#'
#' @references <https://stackoverflow.com/q/4752275>
#'
#' @export
#'
#' @examples
#' #----------------------------------------------------------------------------
#' # is_rep() examples
#' #----------------------------------------------------------------------------
#' library(bkmisc)
#'
#' x <- c("a", "b")
#' y <- c("a", "a")
#' is_rep(x)
#' is_rep(y)
#'
#' x <- 1:2
#' is_rep(x)
#' is_rep(x, tol = .Machine$double.eps ^ 0.5)
#'
#' x <- c(1, 1, NA)
#' is_rep(x)
#' is_rep(x, tol = .Machine$double.eps ^ 0.5)
#'
is_rep <- function(x, tol = NULL) {
  if(is.null(tol)) {
    length(unique(x)) == 1
  } else {
    abs(max(x, na.rm = TRUE) - min(x, na.rm = TRUE)) < tol
  }
}

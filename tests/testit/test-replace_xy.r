library(testit)

#===============================================================================
# Data for use throughout this file.
#===============================================================================
# replace_xy() doesnt work with Date columns.
df <- bkdat::as_df(
  list(
    #POSIXct = as.POSIXct(c("2017-08-13", "2017-08-14")),
    #POSIXlt = as.POSIXlt(c("2017-08-13", "2017-08-14")),
    #Date = as.Date(c("2017-08-13", "2017-08-14")),
    numeric = c(1, 2),
    integer = 3:4,
    character = letters[1:2],
    missing = rep(c(1, NA)),
    infinte = rep(c(2, Inf)),
    logical = rep(c(T, F)),
    factor = factor(letters[1:2]),
    list = replicate(2, list(a = data.frame(b = c(1, 2))))
  )
)
df_NA <- bkdat::as_df(
  list(
    #POSIXct = as.POSIXct(c("2017-08-13", "2017-08-14")),
    #POSIXlt = as.POSIXlt(c("2017-08-13", "2017-08-14")),
    #Date = as.Date(c("2017-08-13", "2017-08-14")),
    numeric = c(1, NA),
    integer = c(1L, NA),
    character = c("a", NA),
    infinte = c(NA, Inf),
    logical = c(T, NA),
    factor = factor(c("a", NA)),
    list = list(NA, list(a = data.frame(b = c(1, 2))))
  )
)
df_Inf <- bkdat::as_df(
  list(
    #POSIXct = as.POSIXct(c("2017-08-13", "2017-08-14")),
    #POSIXlt = as.POSIXlt(c("2017-08-13", "2017-08-14")),
    #Date = as.Date(c("2017-08-13", "2017-08-14")),
    numeric = c(1, Inf),
    integer = c(1L, Inf),
    character = c("a", Inf),
    infinte = c(Inf, Inf),
    logical = c(T, Inf),
    factor = factor(c("a", Inf)),
    list = list(Inf, list(a = data.frame(b = c(1, 2))))
  )
)


m <- matrix(
  c(NA, 2, 3, 4, 5, Inf),
  nrow = 3,
  ncol = 2
)

m_char <- matrix(
  c(NA, "a", "b", "c", "d", "e"),
  nrow = 3,
  ncol = 2
)

ndf <- bkdat::nest_by(mtcars, "carb")
gdf <- bkdat::group_by(mtcars, "carb")

#===============================================================================
# Check errors and warnings.
#===============================================================================
assert(
  "replace_xy() errors with unknown methods.",
  has_error(replace_xy(list()))
)

#-------------------------------------------------------------------------------
# matrices
#-------------------------------------------------------------------------------
assert(
  "replace_xy() (matrix) errors with missing y.",
  has_error(replace_xy(m, x = 2))
)

assert(
  "replace_xy() (matrix) errors with missing x.",
  has_error(replace_xy(m, y = 2))
)

assert(
  "replace_xy() (matrix) errors for coercion.",
  has_error(replace_xy(m, 2, "y")),
  has_error(replace_xy(m, 2, 3L)),
  has_error(replace_xy(m, "2", "3")),
  has_error(replace_xy(m_char, "a", 2))
)

#-------------------------------------------------------------------------------
# dataframes
#-------------------------------------------------------------------------------
assert(
  "replace_xy() (data.frame) errors with missing y.",
  has_error(replace_xy(df, x = 2))
)

assert(
  "replace_xy() (data.frame) errors with missing x.",
  has_error(replace_xy(df, y = 2))
)

#===============================================================================
# Check other function properties.
#===============================================================================
#-------------------------------------------------------------------------------
# replace_xy works as expected with dataframes.
#-------------------------------------------------------------------------------
assert(
  "replace_xy() converts NA_integer_ to 2L.",
  identical(replace_xy(df_NA, NA_integer_, 2L)$integer, 1:2),
  identical(replace_xy(df_NA, NA_integer_, 2L)$numeric, c(1, NA))
)
assert(
  "replace_xy() converts NA_real_ to 2.",
  identical(replace_xy(df_NA, NA_real_, 2)$numeric, c(1, 2)),
  identical(replace_xy(df_NA, NA_real_, 2)$integer, c(1L, NA))
)
assert(
  "replace_xy() converts NA_character_ to '2'.",
  identical(replace_xy(df_NA, NA_character_, "2")$character, c("a", "2")),
  identical(replace_xy(df_NA, NA_real_, 2)$integer, c(1L, NA))
)
assert(
  "replace_xy() converts NA to FALSE.",
  identical(replace_xy(df_NA, NA, FALSE)$logical, c(T, F)),
  identical(replace_xy(df_NA, NA, FALSE)$integer, c(1L, NA))
)
assert(
  "replace_xy() converts factor <NA> to factor 'a'.",
  identical(replace_xy(df_NA, factor(NA), factor("a"))$factor, factor(c("a", "a"))),
  identical(replace_xy(df_NA, factor(NA), factor("a"))$integer, c(1L, NA))
)
assert(
  "replace_xy() converts list(NA) to list(2).",
  identical(replace_xy(df_NA, list(NA), list(2))$list, list(2, list(a = data.frame(b = c(1, 2))))),
  identical(replace_xy(df_NA, list(NA), list(2))$integer, c(1L, NA))
)

#-------------------------------------------------------------------------------
# replace_xy works as expected with matrices.
#-------------------------------------------------------------------------------
assert(
  "replace_xy() converts NA to 999.",
  identical(replace_xy(m, NA_real_, 999)[1, 1], 999),
  identical(replace_xy(m, NA_character_, 999)[1, 1], 999),
  identical(replace_xy(m, NA, 999)[1, 1], 999)
)
assert(
  "replace_xy() converts '2' to NA_real_.",
  identical(replace_xy(m, "2", NA_real_)[1, 1], NA_real_)
)

#-------------------------------------------------------------------------------
# replace_xy works as expected with nest_df and group_df.
#-------------------------------------------------------------------------------
assert(
  "replace_xy() works as expected with nest_df and group_df.",
  identical(c(4, 1, 3, 3, 6, 8), replace_xy(ndf, 2, 3)$carb),
  identical(c(4, 4, 1, 1, 3), replace_xy(gdf, 2, 3)$carb[1:5])
)

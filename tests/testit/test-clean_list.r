library(testit)

#===============================================================================
# Data for use throughout this file.
#===============================================================================
#-------------------------------------------------------------------------------
# Check clean_list and any_empty_list.
#-------------------------------------------------------------------------------
l1_bad <- list(
  NULL,
  numeric = 1,
  integer = 1L,
  character = "a",
  missing = NA,
  logical = T,
  factor = factor(1),
  list = list(a = list(data.frame(a = c(1, 2, 3)))),
  data.frame = data.frame(a = 1),
  numeric(0),
  list(
    numeric = 1,
    NULL,
    integer = 1L,
    character = "a",
    missing = NA,
    logical = T,
    factor = factor(1),
    numeric(0),
    list = list(a = list(data.frame(a = c(1, 2, 3)))),
    data.frame = data.frame(a = 1)
  ),
  list(
    list(
      numeric(0),
      numeric = 1,
      integer = 1L,
      character = "a",
      missing = NA,
      logical = T,
      factor = factor(1),
      list = list(a = list(data.frame(a = c(1, 2, 3)))),
      data.frame = data.frame(a = 1),
      NULL
    )
  ),
  list(
    list(
      list(
        numeric = 1,
        integer = 1L,
        character = "a",
        missing = NA,
        numeric(0),
        logical = T,
        factor = factor(1),
        list = list(a = list(data.frame(a = c(1, 2, 3)))),
        NULL,
        data.frame = data.frame(a = 1)
      )
    ),
    NULL
  )
)

l1_good <- list(
  numeric = 1,
  integer = 1L,
  character = "a",
  missing = NA,
  logical = T,
  factor = factor(1),
  list = list(a = list(data.frame(a = c(1, 2, 3)))),
  data.frame = data.frame(a = 1),
  list(
    numeric = 1,
    integer = 1L,
    character = "a",
    missing = NA,
    logical = T,
    factor = factor(1),
    list = list(a = list(data.frame(a = c(1, 2, 3)))),
    data.frame = data.frame(a = 1)
  ),
  list(
    list(
      numeric = 1,
      integer = 1L,
      character = "a",
      missing = NA,
      logical = T,
      factor = factor(1),
      list = list(a = list(data.frame(a = c(1, 2, 3)))),
      data.frame = data.frame(a = 1)
    )
  ),
  list(
    list(
      list(
        numeric = 1,
        integer = 1L,
        character = "a",
        missing = NA,
        logical = T,
        factor = factor(1),
        list = list(a = list(data.frame(a = c(1, 2, 3)))),
        data.frame = data.frame(a = 1)
      )
    )
  )
)

l2 <- list(
  numeric = 1,
  integer = 1L,
  character = "a",
  missing = NA,
  logical = T,
  factor = factor(1),
  dataframe = data.frame(a = 1, b = 2)
)

#===============================================================================
# Check errors and warnings.
#===============================================================================
assert(
  "any_empty_list() returns error if x isn't a list.",
  has_error(any_empty_list(data.frame()))
)

#===============================================================================
# Check other function properties.
#===============================================================================
assert(
  "clean_list() returns x if x isn't a list",
  identical(clean_list(1), 1)
)

assert(
  "clean_list() removes unwanted elements.",
  identical(
    l1_good,
    clean_list(l1_bad)
  )
)

assert(
  "clean_list() doesn't modify good lists.",
  identical(
    l2,
    clean_list(l2)
  )
)

assert(
  "any_empty_list() correctly identifies bad lists.",
  any_empty_list(l1_bad)
)

assert(
  "any_empty_list() correctly identifies good lists.",
  !any_empty_list(l2),
  !any_empty_list(l1_good)
)

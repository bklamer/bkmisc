library(testit)

#===============================================================================
# Data for use throughout this file.
#===============================================================================
# case for ties in counts
f1 <- factor(c("a", "a", "a", "b", "b", "b", "c"), levels = c("c", "b", "a"))
# no ties in counts
f2 <- factor(c("a", "a", "a", "b", "b", "c"), levels = c("c", "b", "a"))

#===============================================================================
# Check errors and warnings.
#===============================================================================
assert(
  "sort_levels() errors with non-factors.",
  has_error(sort_levels(c("a")))
)

#===============================================================================
# Check other function properties.
#===============================================================================
#-------------------------------------------------------------------------------
# sort_levels works as expected.
#-------------------------------------------------------------------------------
assert(
  "sort_levels() works for ties.",
  identical(
    levels(sort_levels(f1, desc = TRUE, desc_ties = FALSE)),
    c("a", "b", "c")
  ),
  identical(
    levels(sort_levels(f1, desc = TRUE, desc_ties = TRUE)),
    c("b", "a", "c")
  ),
  identical(
    levels(sort_levels(f1, desc = FALSE, desc_ties = FALSE)),
    c("c", "a", "b")
  ),
  identical(
    levels(sort_levels(f1, desc = FALSE, desc_ties = TRUE)),
    c("c", "b", "a")
  )
)

assert(
  "sort_levels() works with no ties.",
  identical(
    levels(sort_levels(f2, desc = TRUE)),
    c("a", "b", "c")
  ),
  identical(
    levels(sort_levels(f2, desc = FALSE)),
    c("c", "b", "a")
  )
)

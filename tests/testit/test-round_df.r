library(testit)

#===============================================================================
# Data for use throughout this file.
#===============================================================================
d_int <- data.frame(
  var1 = 1:3,
  var2 = 4:6,
  var3 = 6:8
)
d_num <- data.frame(
  var1 = c(1.0001, 2, 3),
  var2 = c(4, 5, 6.1234567),
  var3 = c(4, 5, 6)
)
d_num3 <- data.frame(
  var1 = c(1, 2, 3),
  var2 = c(4, 5, 6.123),
  var3 = c(4, 5, 6)
)
d_mix <- bkdat::as_df(
  list(
    POSIXct = as.POSIXct(c("2017-08-13", "2017-08-14")),
    POSIXlt = as.POSIXlt(c("2017-08-13", "2017-08-14")),
    Date = as.Date(c("2017-08-13", "2017-08-14")),
    numeric = c(1.399, 2.599),
    integer = 3:4,
    character = letters[1:2],
    missing = rep(c(1, NA)),
    infinte = rep(c(2, Inf)),
    logical = rep(c(T, F)),
    factor = factor(letters[1:2]),
    list = replicate(2, list(a = data.frame(b = c(1.33, 2))))
  )
)
d_mix2 <- bkdat::as_df(
  list(
    POSIXct = as.POSIXct(c("2017-08-13", "2017-08-14")),
    POSIXlt = as.POSIXlt(c("2017-08-13", "2017-08-14")),
    Date = as.Date(c("2017-08-13", "2017-08-14")),
    numeric = c(1.40, 2.60),
    integer = 3:4,
    character = letters[1:2],
    missing = rep(c(1, NA)),
    infinte = rep(c(2, Inf)),
    logical = rep(c(T, F)),
    factor = factor(letters[1:2]),
    list = replicate(2, list(a = data.frame(b = c(1.33, 2))))
  )
)

m_int <- matrix(
  c(NA, 1:5),
  nrow = 3,
  ncol = 2
)
m_log <- matrix(
  rep(c(NA, T, F), 2),
  nrow = 3,
  ncol = 2
)
m_num <- matrix(
  c(NA, 2.599, 3, 4.34, 5, Inf),
  nrow = 3,
  ncol = 2
)
m_num2 <- matrix(
  c(NA, 2.60, 3, 4.34, 5, Inf),
  nrow = 3,
  ncol = 2
)
m_char <- matrix(
  c(NA, "a", "b", "c", "d", "e"),
  nrow = 3,
  ncol = 2
)

#===============================================================================
# Check errors and warnings.
#===============================================================================
assert(
  "round_df() errors with non-dataframe or non-matrix.",
  has_error(round_df(list()))
)
assert(
  "round_df() errors if 'digits' is not a number.",
  has_error(round_df(m_num, "y")),
  has_error(round_df(d_num, "2"))
)
assert(
  "round_df() (matrix) errors if used on non-numeric data.",
  has_warning(round_df(m_char, 1)),
  has_warning(round_df(m_int, 1)),
  has_warning(round_df(m_log, 1))
)

#===============================================================================
# Check other function properties.
#===============================================================================
#-------------------------------------------------------------------------------
# round_df works as expected with dataframes.
#-------------------------------------------------------------------------------
assert(
  "round_df() works as expected for data frames.",
  identical(d_int, round_df(d_int, 4)),
  identical(d_num3, round_df(d_num, 3)),
  identical(d_mix2, round_df(d_mix, 2))
)

#-------------------------------------------------------------------------------
# round_df works as expected with matrices.
#-------------------------------------------------------------------------------
assert(
  "round_df() works as expected for matrices.",
  has_warning(identical(m_int, round_df(m_int, 3))),
  has_warning(identical(m_log, round_df(m_log, 2))),
  has_warning(identical(m_char, round_df(m_char, 2))),
  identical(m_num2, round_df(m_num, 2))
)

library(testit)

#===============================================================================
# Data for use throughout this file.
#===============================================================================
df <- bkdat::as_df(
  list(
    POSIXct = as.POSIXct(c(NA, "2017-08-13", "2017-08-14")),
    POSIXlt = as.POSIXlt(c("2017-08-13", NA, "2017-08-14")),
    Date = as.Date(c("2017-08-13", "2017-08-14", NA)),
    numeric = c(NA, 2, 1),
    integer = c(3L, NA, 4L),
    character = c(letters[1:2], NA),
    infinite = c(NA, 1, Inf),
    logical = c(TRUE, NA, FALSE),
    factor = factor(c(letters[1:2], NA)),
    list = list(list(a = data.frame(b = c(1, 2))), NA, list(c = data.frame(d = c(3, 4))))
  )
)

#===============================================================================
# Check errors and warnings.
#===============================================================================
assert(
  "capply() errors with non-dataframes.",
  has_error(capply(list()))
)
assert(
  "capply() errors with bad selection criteria.",
  has_error(capply(df, c("a", "b", "c"), is.integer))
)
assert(
  "capply() errors with if you don't pass a function.",
  has_error(capply(df, 1, 1))
)

#===============================================================================
# Check other function properties.
#===============================================================================
assert(
  "capply() works as expected.",
  is.character(capply(df, sapply(df, is.factor), as.character)$factor)
)

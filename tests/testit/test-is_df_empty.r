library(testit)

#===============================================================================
# Check errors and warnings.
#===============================================================================
#-------------------------------------------------------------------------------
# is_df_empty errors if supplied a non-dataframe.
#-------------------------------------------------------------------------------
assert(
  "is_df_empty() errors if supplied a non-dataframe.",
  has_error(is_df_empty(list()))
)

#===============================================================================
# Check other function properties.
#===============================================================================
#-------------------------------------------------------------------------------
# is_df_empty works as expected.
#-------------------------------------------------------------------------------
assert(
  "is_df_empty() works correctly.",
  identical(TRUE, is_df_empty(NULL)),
  identical(TRUE, is_df_empty(data.frame(NULL))),
  identical(TRUE, is_df_empty(data.frame(numeric(0)))),
  identical(FALSE, is_df_empty(data.frame(a = 1)))
)

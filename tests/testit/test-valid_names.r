library(testit)

#===============================================================================
# Data for use throughout this file.
#===============================================================================
d <- data.frame(
  `BaD nAmE_(*iS#^baD  ` = c(1, 2, 3),
  `good_name_is_good` = c(4, 5, 6),
  check.names = FALSE
)
x <- names(d)

#===============================================================================
# Check errors and warnings.
#===============================================================================
assert(
  "valid_names() errors with non-dataframes.",
  has_error(valid_names(d))
)

#===============================================================================
# Check other function properties.
#===============================================================================
#-------------------------------------------------------------------------------
# valid_names works as expected.
#-------------------------------------------------------------------------------
assert(
  "valid_names() works as expected.",
  identical(
    valid_names(x),
    c("bad_name_is_bad", "good_name_is_good")
  )
)

#-------------------------------------------------------------------------------
# valid_names() catches make.names() introducing new issues and 3 rounds duplicates
#-------------------------------------------------------------------------------
assert(
  "valid_names() works as expected.",
  identical(
    valid_names(c("other", "other:", "other_1"), n_rounds = 3),
    c("other", "other_1", "other_1_1")
  )
)

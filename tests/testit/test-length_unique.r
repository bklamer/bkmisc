library(testit)

#-------------------------------------------------------------------------------
# n_unique works as expected.
#-------------------------------------------------------------------------------
assert(
  "length_unique() works correctly.",
  identical(1L, length_unique(10)),
  identical(3L, length_unique(c(1, 2, 3))),
  identical(2L, length_unique(factor(c("a", "a", "b", "b"))))
)
